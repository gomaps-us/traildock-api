'use strict';

var geojson = {

  isMultiLineString: function (value) {
    if (value instanceof Array) {
      for (var i = 0; i < value.length; i++) {
        if (!geojson.isLineString(value[i])) {
          return false;
        }
      }
      return true;
    }
    return false;
  },

  isLineString: function (value) {
    if (value instanceof Array) {
      for (var i = 0; i < value.length; i++) {
        if (!geojson.isPoint(value[i])) {
          return false;
        }
      }
      return true;
    }
    return false;
  },

  isPoint: function (value) {
    var shared = require('./shared');
    if (value instanceof Array) {
      if (shared.isNumeric(value[0]) && shared.isNumeric(value[1])) {
        return true;
      }
    }
    return false;
  },

  getType: function (osmTypes, total) {
    if (!(osmTypes instanceof Array)) {
      if (osmTypes == 'nodes') return 'Point';
      if (osmTypes == 'ways') return 'LineString';
      if (osmTypes == 'relations') return 'MultiLineString';
    }
    if (osmTypes.length == 1 && total == 1) {
      return (osmTypes[0] == 'nodes') ? 'Point' : (osmTypes[0] == 'ways' ? 'LineString' : 'MultiLineString');
    }
    return 'MultiLineString';
  },

  getCoordinatesAsList: function (coordinates) {
    var asList  = [];
    if (geojson.isPoint(coordinates)) {
      return [coordinates];
    }
    coordinates.forEach(function (item) {
      if (geojson.isPoint(item)) {
        asList.push(item);
      } else {
        asList = asList.concat(geojson.getCoordinatesAsList(item));
      }
    });
    return asList;
  },

  elevationPrimarilyPresent: function (coordinates) {
    if (geojson.isPoint(coordinates) && coordinates.length > 2) return true;
    var withElevation = 0;
    var coordinatesList = geojson.getCoordinatesAsList(coordinates);
    coordinatesList.forEach(function (coordinate) {
      if (coordinate.length > 2) {
        withElevation++;
      }
    });
    return ((withElevation/coordinatesList.length) >= .75);
  },

  addElevation: function (coordinates, longitude, latitude, elevation) {
    var shared = require('./shared');
    if (geojson.isPoint(coordinates)) {
      if (shared.isApproximatelyEqual(coordinates[0], longitude) && shared.isApproximatelyEqual(coordinates[1], latitude)) {
        coordinates[2] = elevation;
      }
    } else if (geojson.isLineString(coordinates)) {
      for (var i = 0; i < coordinates.length; i++) {
        if (shared.isApproximatelyEqual(coordinates[i][0], longitude) && shared.isApproximatelyEqual(coordinates[i][1], latitude)) {
          coordinates[i][2] = elevation;
        }
      }
    } else if (geojson.isMultiLineString(coordinates)) {
      for (var i = 0; i < coordinates.length; i++) {
        for (var ii = 0; ii < coordinates[i].length; ii++) {
          if (shared.isApproximatelyEqual(coordinates[i][ii][0], longitude) && shared.isApproximatelyEqual(coordinates[i][ii][1], latitude)) {
            coordinates[i][ii][2] = elevation;
          }
        }
      }
    }
    return coordinates;
  },

};

module.exports = geojson;
