// this module is reserved for filtering/processing data for backwards compatibility
'use strict';

var compatibility = function () {

  this.filterProperties = function (Model, data) {
    switch (Model.modelName.toLowerCase()) {
      case 'report':
        if (data.heading && !data.title) {
          data.title = data.heading;
        }
        data.heading = data.title;
        break;
      case 'trailhead':
      case 'route':
      case 'track':
        if (data.name && !data.title) {
          data.title = data.name;
        }
        data.name = data.title;
        break;
      default:
        // no default
    }
  };

  this.getStandardizedPasswordProperties = function (data) {
    return {
      oldPassword: data.oldPassword,
      password: data.newPassword || data.password,
      passwordConfirm: data.passwordConfirm || data.confirmPassword || data.confirmation,
    }
  };

};

module.exports = new compatibility();
