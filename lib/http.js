'use strict';

var request = require('request');
var Promise = require('bluebird');

var http = function () {

  this.request = function (method, baseUrl, path, auth, data) {
    var options = {
      method: method,
      baseUrl: baseUrl,
      uri: path,
      body: data,
      auth: auth
    };
    return new Promise(function (resolve, reject) {
      request(options, function (error, response, body) {
        if (error) {
          var err = new Error();
          err.status = response && response.statusCode ? response.statusCode : -1;
          err.message = error;
          return reject(err);
        }
        return resolve(response);
      });
    });
  };

}

module.exports = new http();
