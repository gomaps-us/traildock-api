'use strict';

var Promise = require('bluebird');
var geojson  = require('../geojson');

module.exports = function (postgres) {

  this.postgres = postgres;
  var _this     = this;

  this.getGeoJson = function (association) {
    var json      = { type: null, coordinates: [] };
    var promises  = [];
    if (association.source != 'osm') return new Promise(function (resolve, reject) { resolve({}); });
    var osmType = association.type.toLowerCase().trim();
    switch (osmType) {
      case 'node':
        return this.getPoint(association.sid);
        break;
      default:
        return this.getLine(association.sid);
    }
  };

  this.getPoint = function(id) {
    return _this.query(
      "SELECT ST_AsGeoJSON(ST_Transform(way,4326)) as geojson FROM planet_osm_point WHERE osm_id = $1",
      [id]).then(function (results) {
        var json = '';
        results.forEach(function (row) {
          json = JSON.parse(row.geojson);
        });
        return json;
      });
  };

  this.getLine = function(id) {
    return _this.query(
      "SELECT ST_AsGeoJSON(ST_Transform(way,4326)) as geojson FROM planet_osm_line WHERE osm_id = $1 OR osm_id = -$1",
      [id]).then(function (results) {
        var json = '';
        results.forEach(function (row) {
          json = JSON.parse(row.geojson);
        });
        return json;
      });
  };

  this.query = function (query, params) {
    return new Promise(function (resolve, reject) {
      _this.postgres.query(query, params, function (err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  };

};
