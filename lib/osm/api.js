'use strict';

var http      = require('../http');
var xml       = require('xml');
var Promise   = require('bluebird');
var parseXml  = Promise.promisify(require('xml2js').parseString);

var api = function (baseUrl, username, password) {

  var _this       = this;

  this.baseUrl    = baseUrl;
  this.username   = username;
  this.password   = password;
  this.auth       = {
    user: username,
    pass: password
  };

  this.initData = function (type) {
    var data = { osm: [{}] };
    data['osm'][0][type] = [ { _attr: {} } ];
    return data;
  };

  this.setTags = function (element, tags) {
    tags = (tags instanceof Array) ? tags : [];
    tags.push({ key: 'source', value: 'Traildock' });
    tags.forEach(function (tag) {
      element.push({ tag: [{ _attr: { k: tag.key, v: tag.value } }] });
    });
  };

  this.createChangeset = function (tags) {
    var data = _this.initData('changeset');
    _this.setTags(data.osm[0].changeset, tags);
    return http.request('PUT', _this.baseUrl, '/api/0.6/changeset/create', _this.auth, xml(data));
  };

  this.createUpdateNode = function (lat, lon, tags, id, version) {
    var data = _this.initData('node');
    data.osm[0].node[0]._attr.lat = lat;
    data.osm[0].node[0]._attr.lon = lon;
    if (version) {
      data.osm[0].node[0]._attr.version = version;
    }
    if (id) {
      data.osm[0].node[0]._attr.id = id;
    }
    _this.setTags(data.osm[0].node, tags);
    return _this.createChangeset([{ key: 'comment', value: (id ? 'update' : 'add') + ' a trail-related marker' }]).then(function (response) {
      data.osm[0].node[0]._attr.changeset = response.body;
      return http.request('PUT', _this.baseUrl, '/api/0.6/node/' + (id ? id : 'create'), _this.auth, xml(data));
    });
  }

  this.createNode = function (lat, lon, tags) {
    return _this.createUpdateNode(lat, lon, tags);
  };

  this.getNode = function (id, parse) {
    parse = (parse === undefined || parse === null) ? true : parse;
    var res;
    return http.request('GET', _this.baseUrl, '/api/0.6/node/' + id).then(function (response) {
      res = response;
      return parseXml(response.body);
    }).then(function (parsed) {
      if (parse) {
        res.body = parsed;
      }
      return res;
    });
  };

  this.deleteNode = function (id) {
    return _this.getNode(id, false).then(function (response) {
      return http.request('DELETE', _this.baseUrl, '/api/0.6/node/' + id, _this.auth, response.body);
    });
  };

  this.updateNode = function (id, lat, lon, tags) {
    return _this.getNode(id).then(function (response) {
      return _this.createUpdateNode(lat, lon, tags, id, response.body.osm.node[0].$.version);
    });
  };

};

module.exports = function (baseUrl, username, password) {
  return new api(baseUrl, username, password);
};
