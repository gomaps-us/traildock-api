'use strict';

var Promise = require('bluebird');

var osm = function (config) {

  var _this   = this;
  this.api    = require('./api')(config.api.url, config.api.username, config.api.password);

  this.getTagValue = function (key, tags) {
    if (!tags.length) return null;
    for (var i = 0; i < tags.length; i++) {
      if (tags[i].key == key) return (tags[i].value === undefined ? null : tags[i].value);
    }
    return null;
  };

  this.setTagValue = function (key, value, tags) {
    if (!tags.length) return null;
    for (var i = 0; i < tags.length; i++) {
      if (tags[i].key == key) {
        tags[i].value = value;
        return;
      }
    }
    tags.push({ key: key, value: value });
  };

  this.makeOsmTags = function (modelName, data) {
    data.tags = (data.tags instanceof Array) ? data.tags : [];
    var osmTags = [
      { key: 'traildock:user', value: data.createdByUsername },
      { key: 'name', value: (data.title === undefined || data.title === null) ? '' : data.title },
      { key: 'description', value: (data.description === undefined || data.description === null) ? '' : data.description }
    ];
    switch (modelName) {
      case 'facility':
        _this.setTagValue('amenity', 'trail_facility', osmTags);
        break;
      case 'feature':
        _this.setTagValue('natural', 'feature', osmTags);
        break;
      case 'observation':
        _this.setTagValue('observation', ((data.title != '' && data.title != null && data.title != undefined) ? data.title : 'trail'), osmTags);
        break;
      case 'report':
        _this.setTagValue('tourism', 'information', osmTags);
        _this.setTagValue('information', 'trail_report', osmTags);
        break;
      case 'sign':
        _this.setTagValue('tourism', 'information', osmTags);
        _this.setTagValue('information', 'board', osmTags);
        break;
      case 'trailhead':
        _this.setTagValue('amenity', 'trailhead', osmTags);
        break;
      default:
        // no defaults
    }
    data.tags.forEach(function (tag) {
      if (tag.match(/^subtype:/)) {
        var subtype = tag.split(':')[1];
        switch (subtype) {
          case 'campground':
            _this.setTagValue('tourism', 'camp_site', osmTags);
            _this.setTagValue('backcountry', 'yes', osmTags);
            break;
          case 'toilet':
            _this.setTagValue('amenity', 'toilets', osmTags);
            break;
          case 'shelter':
            _this.setTagValue('amenity', 'shelter', osmTags);
            break;
          case 'water':
            _this.setTagValue('amenity', 'drinking_water', osmTags);
            break;
          case 'trailpost':
            _this.setTagValue('information', 'guidepost', osmTags);
            break;
          default:
            // no default
        }
      }
    });
    return osmTags;
  };

  this.getAssociation = function (type, associations) {
    for (var i = 0; i < associations.length; i++) {
      if (associations[i].source == 'osm' && associations[i].type == type) {
        return associations[i];
      }
    }
    return null;
  };

  this.syncTo = function (type, Model, data) {
    if (config.sync !== true) return new Promise(function (resolve, reject) { return resolve(null); });;
    var err;
    var create = function (data, Model) {
      return _this.api.createNode(data.geometry.coordinates[1],
                                  data.geometry.coordinates[0],
                                  _this.makeOsmTags(Model.modelName, data)).then(function (response) {
        if (response.statusCode == 200) {
          console.log('Saved new node to OSM: ' + response.body);
          data.associations.push({
            source: 'osm',
            type: 'node',
            sid: response.body
          });
        } else {
          err = new Error();
          err.status = response.statusCode;
          err.message = response.body;
          throw err;
        }
      });
    };
    if ((data.geometry instanceof Object) && data.geometry.type == 'Point') {
      data.associations = (data.associations instanceof Array) ? data.associations : [];
      switch (type) {
        case 'delete':
          return _this.api.deleteNode(_this.getAssociation('node', data.associations).sid);
          break;
        case 'create':
          return create(data, Model);
          break;
        case 'update':
          var association = _this.getAssociation('node', data.associations);
          if (association == null) {
            return create(data, Model);
          }
          return _this.api.updateNode(association.sid,
                                      data.geometry.coordinates[1],
                                      data.geometry.coordinates[0],
                                      _this.makeOsmTags(Model.modelName, data)).then(function (response) {
            if (response.statusCode == 200) {
              console.log('Updated node to OSM node ' + association.sid + ' to version: ' + response.body);
              var foundAssociation = false;
              for (var i = 0; i < data.associations.length; i++) {
                if (data.associations[i].source == 'osm' && data.associations[i].type == 'node' && data.associations[i].sid == association.sid) {
                  foundAssociation = true;
                }
              }
              if (!foundAssociation) {
                data.associations.push({
                  source: 'osm',
                  type: 'node',
                  sid: association.sid
                });
              }
            } else {
              err = new Error();
              err.status = response.statusCode;
              err.message = response.body;
              throw err;
            }
          });
          break;
        default:
          err = new Error();
          err.status = 500;
          err.message = 'Unknown save operation type: ' + type;
          return new Promise(function (resolve, reject) { return reject(err); });
      }
    } else {
      return new Promise(function (resolve, reject) { return resolve(null); });
    }
  };

}

module.exports = function (config) {
  return new osm(config);
};
