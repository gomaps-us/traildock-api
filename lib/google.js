'use strict';

var Promise   = require('bluebird');
var geojson   = require('./geojson');
var http      = require('./http');

module.exports = function (apiUri, apiKey) {

  this.apiUri       = apiUri;
  this.apiKey       = apiKey;
  var _this         = this;

  this.getElevation = function (geometry) {
    var locations = [];
    geojson.getCoordinatesAsList(geometry.coordinates).forEach(function (coordinate) {
      locations.push(coordinate[1] + ',' + coordinate[0]);
    });
    locations = locations.join('|');
    return http.request('GET', _this.apiUri, '/elevation/json?key=' + _this.apiKey + '&locations=' + locations).then(function (response) {
      var body = JSON.parse(response.body);
      if (body.status != 'OK') throw body.status;
      var withElevation = [];
      body.results.forEach(function (result) {
        withElevation.push([result.location.lng, result.location.lat, result.elevation]);
      });
      if (withElevation.length == 1) withElevation = withElevation[0];
      return withElevation;
    });
  };

};
