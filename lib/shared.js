'use strict';

var geojson           = require('./geojson');
var geojsonvalidator  = require('geojson-validation');
var GoogleLib         = require('./google');
var extend            = require('extend');
var config            = require('config');
var compatibility     = require('./compatibility');
var osm               = require('./osm')(config.osm);

var shared = function () {

  var _this   = this;

  this.disableMethods = function (Model, enabledMethods) {
    if (!enabledMethods) enabledMethods = [];
    Model.sharedClass.methods().forEach(function (method) {
      method.shared = enabledMethods.indexOf(method.name) > -1;
    });
  };

  this.setContextOptions = function (context) {
    if (context.Model && context.Model.app.get('_options')) {
      context.options = context.Model.app.get('_options');
      context.Model.app.set('_options', null);
    }
  };

  this.addHooks = function (Model) {
    Model.observe('before save', _this.beforeSave);
    Model.observe('before delete', _this.beforeDelete);
    Model.beforeRemote('find', _this.beforeFind);
    Model._checkAccessOriginal = Model.checkAccess;
    Model.checkAccess = function (token, modelId, sharedMethod, context, callback) {
      if (modelId !== undefined && modelId !== null) {
        Model.findById(modelId, function (err, result) {
          if (err) callback(err);
          var userId = (context.req.accessToken !== null) ? context.req.accessToken.userId : '';
          if (result && result.public === false && result.createdBy && result.createdBy.toString() != userId.toString()) {
            err = new Error();
            err.status = err.statusCode = 403;
            err.message = 'Forbidden';
            return callback(err);
          }
          Model._checkAccessOriginal(token, modelId, sharedMethod, context, callback);
        });
      } else {
        Model._checkAccessOriginal(token, modelId, sharedMethod, context, callback);
      }
    };
  };

  this.setDefaultFilter = function (filter, accessToken) {
    if (!filter) filter = {};
    if (!filter.where) filter.where = {};
    if (!filter.where.or) filter.where.or = [];
    filter.where.or.push({ public: true });
    filter.where.or.push({ createdBy: (accessToken ? accessToken.userId : '') });
    if (!filter.where.state) {
      filter.where.state = 'active';
    }
    return filter;
  };

  this.beforeFind = function (context, model, next) {
    _this.setContextOptions(context);
    context.args.filter = _this.setDefaultFilter(context.args.filter, context.req.accessToken);
    next();
  };

  this.beforeDelete = function (context, next) {
    osm.syncTo('delete', context.Model, context.where).then(function (response) {
      return next();
    }).catch(function (error) {
      return next(error);
    });
  };

  this.beforeSave = function (context, next) {
    _this.setContextOptions(context);
    var data = context.instance ? context.instance : context.data;
    if (data.public && data.public != true) {
      var err = new Error();
      err.status = err.statusCode = 400;
      err.message = 'Private data is not supported yet';
      return next(err);
    }
    if (context.isNewInstance) {
      _this.setProperties(context, next);
    } else {
      if (context.currentInstance) {
        context.existing = context.currentInstance;
        _this.setProperties(context, next);
      } else {
        context.Model.findById(context.instance.id, function (err, existing) {
          if (err) return next(err);
          context.existing = existing;
          _this.setProperties(context, next);
        });
      }
    }
  };

  this.setProperties = function (context, next) {
    if (!('accessToken' in context.options)) return next('accessToken not found in request that requires a user');
    context.Model.app.models.Member.findById(context.options.accessToken.userId, function (err, user) {
      if (err) return next(err);
      if (context.isNewInstance) {
        context.instance.createdBy          = user.id;
        context.instance.createdByUsername  = user.username;
        context.instance.createdAt          = Date.now();
      }
      if (context.instance) {
        if (context.existing) {
          context.instance.createdBy          = context.existing.createdBy;
          context.instance.createdByUsername  = context.existing.createdByUsername;
          context.instance.createdAt          = context.existing.createdAt;
        }
        context.instance.updatedBy           = user.id;
        context.instance.updatedByUsername  = user.username;
        context.instance.updatedAt          = Date.now();
        _this.processFiles(context, context.instance, user.id, next);
      } else {
        if (context.existing) {
          context.data.createdBy          = context.existing.createdBy;
          context.data.createdByUsername  = context.existing.createdByUsername;
          context.data.createdAt          = context.existing.createdAt;
        }
        context.data.updatedBy          = user.id;
        context.data.updatedByUsername  = user.username;
        context.data.updatedAt          = Date.now();
        _this.processFiles(context, context.data, user.id, next);
      }
    });
  };

  this.processFiles = function (context, data, userId, next) {
    if (data.files && data.files instanceof Array) {
      context._files  = data.files;
      context._userId = userId;
      context.Model.app.models.Files.upload(context, function (err, results) {
        if (err) return next(err);
        for (var i = 0; i < data.files.length; i++) {
          if (data.files[i].match(/^data:/g) !== null) {
            delete data.files[i];
          }
        }
        results.forEach(function (result) {
          data.files.push(result.location);
        });
        return _this.setGeometry(context, context.Model, data, next);
      });
    } else {
      return _this.setGeometry(context, context.Model, data, next);
    }
  };

  this.setGeometry = function (context, Model, data, next) {
    var associationLib = null;
    if (!(data.geometry instanceof Object)) data.geometry = {};
    var association = (data.associations instanceof Array && data.associations.length > 0) ?
                      data.associations[0] :
                      ((data.association instanceof Object) ? data.association : null);
    if (!data.geometry.coordinates && association) {
      // this is a record w/o geometry explicitly provided, but there is an association
      // We'll take the first association and see if we can go get some geometry data from it
      associationLib = new (require('./' + association.source).connection)(Model.app.dataSources[association.source].connector);
      associationLib.getGeoJson(association).then(function (result) {
        if (geojsonvalidator.valid(result)) {
          data.geometry = result;
        } else {
          data.geometry = undefined;
          return next();
        }
        return _this.setFromGeometry(context, Model, data, next);
      }).catch(function (err) {
        return next(err);
      });
    } else {
      if (!geojsonvalidator.valid(data.geometry)) {
        data.geometry = undefined;
        return next();
      }
      return _this.setFromGeometry(context, Model, data, next);
    }
  };

  this.setFromGeometry = function (context, Model, data, next) {
    if (Model.setFromGeometry && data.geometry && data.geometry.coordinates) {
      Model.setFromGeometry(Model, data).then(function (result) {
        return _this.setElevation(context, Model, result, next);
      }).catch(function (err) {
        return next(err);
      });
    } else {
      return _this.setElevation(context, Model, data, next);
    }
  };

  this.setElevation = function (context, Model, data, next) {
    var google  = new GoogleLib(config.get('google.api.uri'), config.get('google.api.key'));
    if (!(data.geometry instanceof Object) || !(data.geometry.coordinates instanceof Array)) return next();
    if (geojson.elevationPrimarilyPresent(data.geometry.coordinates)) return next();
    google.getElevation(data.geometry).then(function (withElevation) {
      if (data.geometry.type == 'Point') {
        data.geometry.coordinates = geojson.addElevation(data.geometry.coordinates, withElevation[0], withElevation[1], withElevation[2]);
      } else {
        for (var i = 0; i < withElevation.length; i++) {
          data.geometry.coordinates = geojson.addElevation(data.geometry.coordinates, withElevation[i][0], withElevation[i][1], withElevation[i][2]);
        }
      }
      compatibility.filterProperties(Model, data);
      return _this.syncToExternalSources(context, Model, data, next);
    }).catch(function (err) {
      return next(err);
    });
  };

  this.syncToExternalSources = function (context, Model, data, next) {
    if (context.isNewInstance) {
      osm.syncTo('create', Model, data).then(function (response) {
        return next();
      }).catch(function (error) {
        return next(error);
      });
    } else {
      if (data.state == 'archived') {
        osm.syncTo('delete', Model, data).then(function (response) {
          return next();
        }).catch(function (error) {
          return next(error);
        });
      } else {
        osm.syncTo('update', Model, data).then(function (response) {
          return next();
        }).catch(function (error) {
          return next(error);
        });
      }
    }
  };

  this.getDefaultWithinDefinition = function (Model) {
    return {
      description: 'Performs a geo query to find any ' + (Model ? Model.modelName : 'object') + ' within the bounds',
      http: { verb: 'get', path: '/within' },
      accessType: 'READ',
      accepts: [
        { arg: 'southWestLatitude', type: 'number', required: true },
        { arg: 'southWestLongitude', type: 'number', required: true },
        { arg: 'northEastLatitude', type: 'number', required: true },
        { arg: 'northEastLongitude', type: 'number', required: true },
        { arg: 'filter', type: 'object', description:
          'Filter defining fields and include - must be a JSON-encoded string (' +
          '{"something":"value"})' },
        { arg: 'options', type: 'object', http: 'optionsFromRequest' },
      ],
      returns: [
        { arg: 'data', type: [(Model ? Model.modelName : 'object')], root: true },
      ],
    };
  };

  this.addGeometryRemoteMethods = function (Model) {
    Model.remoteMethod('within', _this.getDefaultWithinDefinition(Model));
    Model.within = function (southWestLatitude, southWestLongitude, northEastLatitude, northEastLongitude, filter, options, cb) {
      filter = _this.setDefaultFilter(filter, options.accessToken);
      filter.where.geometry = {
        'geoWithin': {
          '$geometry': {
            'type': 'Polygon',
            'coordinates': [[
              [southWestLongitude, southWestLatitude],
              [southWestLongitude, northEastLatitude],
              [northEastLongitude, northEastLatitude],
              [northEastLongitude, southWestLatitude],
              [southWestLongitude, southWestLatitude],
            ]],
          },
        },
      };
      Model.find(filter, function (err, results) {
        if (err) return cb(err);
        cb(null, (results ? results: []));
      });
    };
  };

  this.addCommonRemoteMethods = function (Model) {
    Model.updatePartial = function (id, data, options, cb) {
      Model.findById(id, function (err, instance) {
        if (err) return cb(err);
        if (Model.modelName.toLowerCase() == 'member' && instance.id.toString() != options.accessToken.userId.toString()) {
          err = new Error();
          err.status = 401;
          err.message = 'Unauthorized';
          return cb(err);
        }
        for (var key in data) {
          if (instance.hasOwnProperty(key) && (data[key] instanceof Object) && instance[key] && (instance[key] instanceof Object)) {
            instance[key] = Object.assign(instance[key], data[key]);
          } else if (data.hasOwnProperty(key)) {
            instance[key] = data[key];
          }
        }
        Model.app.set('_options', options);
        instance.updateAttributes(instance, function (err, updated) {
          if (err) return cb(err);
          cb(null, updated);
        });
      });
    };
    Model.remoteMethod('updatePartial', {
      isStatic: true,
      description: 'Updates just the fields submitted in the data for a given model instance',
      http: { verb: 'put', path: '/:id/update' },
      accepts: [
        { arg: 'id', type: 'string', http: { source: 'path' }, required: true },
        { arg: 'data', type: 'object', http: { source: 'body' }, required: true },
        { arg: 'options', type: 'object', http: 'optionsFromRequest' },
      ],
      returns: { arg: 'data', type: Model.modelName, root: true },
    });
    Model.remoteMethod('create', _this.getCreateDefinition(Model));
  };

  this.getCreateDefinition = function (Model) {
    return {
      description: 'Create a new instance of the model and persist it into the data source.',
      accessType: 'WRITE',
      accepts: [
        {
          arg: 'data', type: 'object', model: Model.modelName, allowArray: true,
          description: 'Model instance data',
          http: { source: 'body' },
        },
        { arg: 'options', type: 'object', http: 'optionsFromRequest' },
      ],
      returns: { arg: 'data', type: Model.modelName, root: true },
      http: { verb: 'post', path: '/', status: 200 },
    };
  }

  this.isApproximatelyEqual = function (a, b, epsilon) {
    if (!epsilon) epsilon = 0.001;
    if (Math.abs(a - b) <= 0.001) return true;
    return false;
  };

  this.isNumeric = function (value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
  };

  // the path w/in a bucket
  this.makeS3FilePath = function (basePath, userId, fileName) {
    var path = basePath;
    if (path.substring(path.length - 1) != '/') path += '/';
    path += userId + '/' + Math.round(+new Date()/1000) + '-' + fileName;
    return path;
  };

};

module.exports = new shared();
