'use strict';

var chai    = require('chai');
var expect  = chai.expect;
var assert  = chai.assert;
var geojson = require('../lib/geojson');

describe('lib/geojson', function () {

  it('getType() should return Point for a single node', function (done) {
    expect(geojson.getType(['nodes'], 1)).to.equal('Point');
    done();
  });

  it('isPoint() should work in detecting whether a point or not', function (done) {
    expect(geojson.isPoint([123, 209])).to.equal(true);
    expect(geojson.isPoint('no')).to.equal(false);
    expect(geojson.isPoint([[123, 209], [123, 30877]])).to.equal(false);
    expect(geojson.isPoint([123, 'no'])).to.equal(false);
    expect(geojson.isPoint(12343)).to.equal(false);
    done();
  });

  it('isLineString() should work in detecting whether a LineString or not', function (done) {
    expect(geojson.isLineString([[123, 209], [8473, 8744]])).to.equal(true);
    expect(geojson.isLineString('no')).to.equal(false);
    expect(geojson.isLineString([123, 209])).to.equal(false);
    expect(geojson.isLineString([[123, 'no'], [188, 987]])).to.equal(false);
    done();
  });

  it('isMultiLineString() should work in detecting whether a MultiLineString or not', function (done) {
    expect(geojson.isMultiLineString([[[123, 209], [8473, 8744]], [[1111, 2222], [8746, 9272]]])).to.equal(true);
    expect(geojson.isMultiLineString('no')).to.equal(false);
    expect(geojson.isMultiLineString([123, 209])).to.equal(false);
    expect(geojson.isMultiLineString([[[123, 'no'], [8272763, 2982]], [[188, 987], [92782, 20282]]])).to.equal(false);
    done();
  });

  it('getCoordinatesAsList() should return a single coordinate in an array for a point', function (done) {
    expect(geojson.getCoordinatesAsList([123, 124])).to.deep.equal([[123, 124]]);
    done();
  });

  it('getCoordinatesAsList() should return a list of single-level-deep coordinates for a MultiLineString', function (done) {
    expect(geojson.getCoordinatesAsList(
      [[[123, 124], [222, 333]], [[987, 787], [9888, 7847]]])
    ).to.deep.equal(
      [[123, 124], [222, 333], [987, 787], [9888, 7847]]
    );
    done();
  });

  it('elevationPrimarilyPresent() should work', function (done) {
    expect(geojson.elevationPrimarilyPresent(
      [[[123, 124], [222, 333]], [[987, 787], [9888, 7847]]])
    ).to.equal(false);
    expect(geojson.elevationPrimarilyPresent(
      [[[123, 124, 23], [222, 333, 23]], [[987, 787, 56], [9888, 7847, 56]]])
    ).to.equal(true);
    expect(geojson.elevationPrimarilyPresent(
      [[[123, 124, 23], [222, 333, 23]], [[987, 787, 56], [9888, 7847]]])
    ).to.equal(true);
    done();
  });

  it('addElevation() should work', function (done) {
    // elevation for a point present
    expect(geojson.addElevation([1, 2], 1, 2, 3)).to.deep.equal([1, 2, 3]);
    // elevation for a point not present
    expect(geojson.addElevation([1, 2], 1, 3, 3)).to.deep.equal([1, 2]);
    // elevation for a linestring point present
    expect(geojson.addElevation([[1, 2], [3, 4]], 3, 4, 3)).to.deep.equal([[1, 2], [3, 4, 3]]);
    // elevation for a linestring point not present
    expect(geojson.addElevation([[1, 2], [3, 4]], 5, 6, 3)).to.deep.equal([[1, 2], [3, 4]]);
    // elevation for a multilinestring point present
    expect(geojson.addElevation([[[1, 2], [3, 4]], [[5, 6], [7, 8]]], 5, 6, 3)).to.deep.equal([[[1, 2], [3, 4]], [[5, 6, 3], [7, 8]]]);
    // elevation for a multilinestring point not present
    expect(geojson.addElevation([[[1, 2], [3, 4]], [[5, 6], [7, 8]]], 9, 10, 3)).to.deep.equal([[[1, 2], [3, 4]], [[5, 6], [7, 8]]]);
    done();
  });

});
