'use strict';

var chai          = require('chai');
var expect        = chai.expect;
var assert        = chai.assert;
var compatibility = require('../lib/compatibility');

describe('lib/compatibility', function () {

  it('filterProperties should return appropriate heading and title for just heading in a report', function (done) {
    var Model = {
      modelName: 'report'
    };
    var data = {
      heading: 'test heading'
    };
    compatibility.filterProperties(Model, data);
    expect(data.heading).to.equal('test heading');
    expect(data.title).to.equal('test heading');
    done();
  });

  it('filterProperties should return appropriate name and title for just name in a trailhead', function (done) {
    var Model = {
      modelName: 'trailhead'
    };
    var data = {
      name: 'test name'
    };
    compatibility.filterProperties(Model, data);
    expect(data.name).to.equal('test name');
    expect(data.title).to.equal('test name');
    done();
  });

  it('filterProperties should return appropriate name and title for just name in a track', function (done) {
    var Model = {
      modelName: 'track'
    };
    var data = {
      name: 'test name'
    };
    compatibility.filterProperties(Model, data);
    expect(data.name).to.equal('test name');
    expect(data.title).to.equal('test name');
    done();
  });

  it('filterProperties should return appropriate name and title for just name in a route', function (done) {
    var Model = {
      modelName: 'route'
    };
    var data = {
      name: 'test name'
    };
    compatibility.filterProperties(Model, data);
    expect(data.name).to.equal('test name');
    expect(data.title).to.equal('test name');
    done();
  });

  it('getStandardizedPasswordProperties should return appropriate values for structure of create', function (done) {
    var properties = compatibility.getStandardizedPasswordProperties({
      password: 'password',
      passwordConfirm: 'passwordConfirm'
    });
    expect(properties.password).to.equal('password');
    expect(properties.passwordConfirm).to.equal('passwordConfirm');
    done();
  });

  it('getStandardizedPasswordProperties should return appropriate values for initial structure of update password', function (done) {
    var properties = compatibility.getStandardizedPasswordProperties({
      oldPassword: 'old-password',
      newPassword: 'new-password',
      confirmPassword: 'confirm-password',
    });
    expect(properties.oldPassword).to.equal('old-password');
    expect(properties.password).to.equal('new-password');
    expect(properties.passwordConfirm).to.equal('confirm-password');
    done();
  });

  it('getStandardizedPasswordProperties should return appropriate values for initial structure of complete reset password', function (done) {
    var properties = compatibility.getStandardizedPasswordProperties({
      password: 'password',
      confirmation: 'confirmation',
    });
    expect(properties.password).to.equal('password');
    expect(properties.passwordConfirm).to.equal('confirmation');
    done();
  });

});
