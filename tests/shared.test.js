'use strict';

var chai    = require('chai');
var expect  = chai.expect;
var assert  = chai.assert;
var shared  = require('../lib/shared');

describe('lib/shared', function () {

  it('isNumeric() should detect when numeric and not', function (done) {
    expect(shared.isNumeric(1)).to.equal(true);
    expect(shared.isNumeric('')).to.equal(false);
    expect(shared.isNumeric('1')).to.equal(true);
    expect(shared.isNumeric(98737)).to.equal(true);
    expect(shared.isNumeric('no')).to.equal(false);
    expect(shared.isNumeric('1.8272')).to.equal(true);
    done();
  });

  it('isApproximatelyEqual should be equal to within epsilon/tolerance', function (done) {
    expect(shared.isApproximatelyEqual(1.00001, 1.00002, 0.0001)).to.equal(true);
    done();
  });

  it('makeS3FilePath should join paths correctly', function (done) {
    expect(shared.makeS3FilePath('api/files', '12989', 'one.png')).to.match(/^api\/files\/12989\/[0-9]+\-one\.png$/);
    expect(shared.makeS3FilePath('api/files/', '12989', 'one.png')).to.match(/^api\/files\/12989\/[0-9]+\-one\.png$/);
    done();
  });

});
