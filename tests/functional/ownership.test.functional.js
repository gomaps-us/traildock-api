'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('Ownership tests', function () {

  it('Created things should be owned by the correct user', function () {

    var members   = [];
    var promises  = [];
    for (var i = 1; i <= 20; i++) {
      members.push({
        username: "testuser_" + i,
        email: "testuser" + i + "@traildock.com",
        firstName: "testuser" + i + "_firstname",
        lastName: "testuser" + i + "_lastname",
        password: "password",
        passwordConfirm: "password"
      });
      var userrun = helper.register(members[members.length - 1], true).then(function (userResponse) {
        var creates = [];
        helper.getCommonCollections().forEach(function (collection) {
          var create = helper.create(userResponse.body.username, 'password', collection,
                                     { title: 'created by ' + userResponse.body.username }).then(function (createResponse) {
            expect(createResponse.body.createdByUsername).to.equal(userResponse.body.username);
          });
          creates.push(create);
        });
        return chakram.all(creates);
      });
      promises.push(userrun);
    }

    return chakram.all(promises);

  });

});
