'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('API Root', function () {
  it('should work for everyone', function () {
    var response = chakram.get(helper.getBaseUrl());
    expect(response).to.have.status(200);
    return chakram.wait();
  });
});
