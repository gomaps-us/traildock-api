'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('Members', function () {

  var id;

  beforeEach(function (done) {
    id = null;
    done();
  });

  afterEach(function (done) {
    if (id) {
      helper.delete('members', id).then(function (response) {
        done();
      }).catch(function (error) {
        throw error;
        done();
      });
    } else {
      done();
    }
  });

  it('GET to /members should contain only the admin user', function () {
    return chakram.get(helper.getBaseUrl() + '/members').then(function (response) {
      expect(response).to.have.status(200);
      expect(response.body.length).to.equal(1);
      expect(response.body[0].username).to.equal('admin');
      return response;
    });
  });

  it('POST to /members should register a user, awaiting email verification', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'testuser@traildock.com',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      id = response.body.id;
      expect(response).to.have.status(200);
      expect(response.body.username).to.equal("testuser@traildock.com");
      expect(response.body.emailVerified).to.equal(false);
    });
  });

  it('POST to /members with a privileged token and forceEmailVerified=true should register a user w/o need for email verification', function () {
    return chakram.post(helper.getBaseUrl() + '/members?privilegedToken=' + helper.getPrivilegedToken(), {
      firstName: 'test',
      lastName: 'user',
      email: 'testuser@traildock.com',
      password: 'password',
      passwordConfirm: 'password',
      forceEmailVerified: true
    }).then(function (response) {
      id = response.body.id;
      expect(response).to.have.status(200);
      expect(response.body.username).to.equal("testuser@traildock.com");
      expect(response.body.emailVerified).to.equal(true);
    });
  });

  it('POST to /members should register a user with a default avatar if that field is not provided', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'testuser@traildock.com',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      id = response.body.id;
      expect(response).to.have.status(200);
      expect(response.body.avatarUrl).to.contain("gravatar.com");
    });
  });

  it('POST to /members should respect passed avatarUrl field', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'automatedtests@traildock.com',
      avatarUrl: '//avatar.com/myavatar',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      id = response.body.id;
      expect(response).to.have.status(200);
      expect(response.body.avatarUrl).to.equal("//avatar.com/myavatar");
    });
  });

  it('POST to /members without first name should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      lastName: 'user',
      email: 'automatedtests@traildock.com',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members without last name should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      email: 'automatedtests@traildock.com',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members without email should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members without a password should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'automatedtests@traildock.com'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members without a passwordConfirm should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'automatedtests@traildock.com',
      password: 'password'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members with mismatched password and passwordConfirm should fail', function () {
    return chakram.post(helper.getBaseUrl() + '/members', {
      firstName: 'test',
      lastName: 'user',
      email: 'automatedtests@traildock.com',
      password: 'password',
      passwordConfirm: 'xxxxxxxxx'
    }).then(function (response) {
      expect(response).to.have.status(422);
    });
  });

  it('POST to /members/reset fails for an invalid email', function () {
    return chakram.post(helper.getBaseUrl() + '/members/reset', {
      email: 'invaliduser@traildock.com'
    }).then(function (response) {
      expect(response).to.have.status(404);
    });
  });

  it('POST to /members/reset gives us success for a valid email', function () {
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password'
    }, true).then(function (response) {
      id = response.body.id;
      return chakram.post(helper.getBaseUrl() + '/members/reset', {
        email: 'validuser@traildock.com'
      });
    }).then(function (response) {
      expect(response).to.have.status(204);
    });
  });

  it('POST to /members/{email}/verify fails for an invalid email', function () {
    return chakram.post(helper.getBaseUrl() + '/members/invaliduser@traildock.com/verify', {
      email: 'invaliduser@traildock.com'
    }).then(function (response) {
      expect(response).to.have.status(404);
    });
  });

  it('POST to /members/{email}/verify gives us a success code for a valid email', function () {
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password'
    }, true).then(function (response) {
      id = response.body.id;
      return chakram.post(helper.getBaseUrl() + '/members/' + response.body.email + '/verify', {
        email: 'validuser@traildock.com'
      });
    }).then(function (response) {
      expect(response).to.have.status(200);
    });
  });

  it('POST to /members/{id}/reset succeeds for a valid token', function () {
    var username;
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password',
    }, true).then(function (response) {
      id        = response.body.id;
      username  = response.body.username;
      return helper.login(username, 'password');
    }).then(function (loginResponse) {
      return chakram.post(helper.getBaseUrl() + '/members/' + id + '/reset?access_token=' + loginResponse.body.id, {
        password: 'newpassword',
        passwordConfirm: 'newpassword',
      });
    }).then(function (response) {
      expect(response).to.have.status(200);
      return helper.login(username, 'newpassword');
    }).then(function (loginResponse) {
      expect(loginResponse.body.id).to.not.be.empty;
    });
  });

  it('POST to /members/{id}/reset fails for an invalid token', function () {
    var username;
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password',
    }, true).then(function (response) {
      id        = response.body.id;
      username  = response.body.username;
      return helper.login(username, 'password');
    }).then(function (loginResponse) {
      return chakram.post(helper.getBaseUrl() + '/members/' + id + '/reset?access_token=xxxxx', {
        password: 'newpassword',
        passwordConfirm: 'newpassword',
      });
    }).then(function (response) {
      expect(response).to.have.status(404);
    });
  });

  it('GET to /members/me returns a user for valid creds', function () {
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password',
    }, true).then(function (response) {
      id = response.body.id;
      return helper.login(response.body.username, 'password');
    }).then(function (loginResponse) {
      return chakram.get(helper.getBaseUrl() + '/members/me?access_token=' + loginResponse.body.id);
    }).then(function (response) {
      expect(response).to.have.status(200);
      expect(response.body.username).to.equal('validuser@traildock.com');
    });
  });

  it('GET to /members/me returns unauthorized for invalid creds', function () {
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password',
    }, true).then(function (response) {
      id = response.body.id;
      return helper.login(response.body.username, 'password');
    }).then(function (loginResponse) {
      return chakram.get(helper.getBaseUrl() + '/members/me?access_token=xxxxxx');
    }).then(function (response) {
      expect(response).to.have.status(401);
    });
  });

  it('POST to /members/update-password succeeds for a valid token', function () {
    var username;
    return helper.register({
      email: 'validuser@traildock.com',
      firstName: 'valid',
      lastName: 'user',
      password: 'password',
      passwordConfirm: 'password',
    }, true).then(function (response) {
      id        = response.body.id;
      username  = response.body.username;
      return helper.login(username, 'password');
    }).then(function (loginResponse) {
      return chakram.post(helper.getBaseUrl() + '/members/update-password?access_token=' + loginResponse.body.id, {
        oldPassword: 'password',
        password: 'newpassword',
        passwordConfirm: 'newpassword',
      });
    }).then(function (response) {
      expect(response).to.have.status(204);
      return helper.login(username, 'newpassword');
    }).then(function (loginResponse) {
      expect(loginResponse.body.id).to.not.be.empty;
    });
  });

  it('GET to /members/{id}/roles returns unauthorized when not logged in', function () {
    return chakram.get(helper.getBaseUrl() + '/members', function (response) {
      return response.body[0].id;
    }).then(function (id) {
      return chakram.get(helper.getBaseUrl() + '/members/' + id + '/roles');
    }).then(function (response) {
      expect(response).to.have.status(401);
    });
  })

  it('GET to /members/{id}/roles returns "admin" for the admin user', function () {
    return helper.login('admin', 'password').then(function (loginResponse) {
      return chakram.get(helper.getBaseUrl() + '/members/' + loginResponse.body.userId + '/roles?access_token=' + loginResponse.body.id);
    }).then(function (response) {
      expect(response).to.have.status(200);
      expect(response.body.length).to.equal(1);
      expect(response.body[0]).to.equal('admin');
    });
  });

  it('POST to /members/autoLogin with invalid privileged token returns forbidden', function () {
    return chakram.post(helper.getBaseUrl() + '/members/autoLogin', {
      privilegedToken: 'aaaabbbb',
      id: '129'
    }).then(function (response) {
      expect(response).to.have.status(403);
    });
  });

  it('POST to /members/autoLogin with invalid user id returns not found', function () {
    return chakram.post(helper.getBaseUrl() + '/members/autoLogin', {
      privilegedToken: helper.getPrivilegedToken(),
      id: '129'
    }).then(function (response) {
      expect(response).to.have.status(404);
    });
  });

  it('POST to /members/autoLogin with valid user ID and privileged token succeeds and returns login response', function () {
    return chakram.get(helper.getBaseUrl() + '/members').then(function (response) {
      return response.body[0].id;
    }).then(function (id) {
      return chakram.post(helper.getBaseUrl() + '/members/autoLogin', {
        privilegedToken: helper.getPrivilegedToken(),
        id: id
      });
    }).then(function (response) {
      expect(response).to.have.status(200);
      expect(response.body.id).to.not.be.empty;
      expect(response.body.userId).to.not.be.empty;
    });
  });

});
