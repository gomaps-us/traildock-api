'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe.skip('OSM association tests', function () {

  var userResponse;

  it('Ensure association happens (after osm sync)', function () {
    var deletes = [];
    return helper.register({
        username: "testuser",
        email: "testuser@traildock.com",
        firstName: "testuser_firstname",
        lastName: "testuser_lastname",
        password: "password",
        passwordConfirm: "password"
      }, true).then(function (response) {
        userResponse = response;
        deletes.push({ collection: 'members', id: userResponse.body.id });
        var data = {
          title: 'created by ' + userResponse.body.username,
          geometry: {
            type: 'Point',
            coordinates: [-106.82135581970216, 38.77469603001716]
          }
        };
        return helper.create(userResponse.body.username, 'password', 'features', data);
      }).then(function (createResponse) {
        deletes.push({ collection: 'features', id: createResponse.body.id });
        expect(createResponse.body.title).to.equal('created by ' + userResponse.body.username);
        expect(createResponse.body.associations.length).to.equal(1);
        expect(createResponse.body.associations[0].source).to.equal('osm');
        expect(createResponse.body.associations[0].type).to.equal('node');
        expect(createResponse.body.associations[0].sid).to.be.above(0);
        var promises = [];
        for (var i = 0; i < deletes.length; i++) {
          promises.push(helper.delete(deletes[i].collection, deletes[i].id));
        }
        return chakram.all(promises);
      });
  });

});
