'use strict';

var child_process     = require('child_process');
var chakram           = require('chakram');
var apiProtocol       = process.env.API_PROTOCOL ? process.env.API_PROTOCOL : 'http';
var apiDomain         = process.env.API_DOMAIN ? process.env.API_DOMAIN : null;
var apiPort           = process.env.API_PORT ? process.env.API_PORT : ':9001';

var helper = function() {

  var _this           = this;

  this.getPrivilegedToken = function () {
    return process.env.PRIVILEGED_TOKEN ? process.env.PRIVILEGED_TOKEN : 'privtoken'
  }

  this.getBaseUrl = function() {
    if (apiDomain == null) {
      apiDomain = child_process.execSync('docker-machine ip').toString().trim();
    }
    return apiProtocol + '://' + apiDomain + apiPort;
  };

  this.register = function (user, verify) {
    verify = verify ? verify : false;
    var path = '/members';
    if (verify) {
      path += '?privilegedToken=' + _this.getPrivilegedToken();
      user.forceEmailVerified = true;
    }
    return chakram.post(_this.getBaseUrl() + path, user);
  };

  this.getValidCoordinate = function () {
    return {
      latitude: 38.59795629797263,
      longitude: -106.65081024169922
    }
  };

  this.getInvalidCoordinate = function () {
    return {
      latitude: 0,
      longitude: 0
    }
  };

  this.getCoordinateAt = function (direction, ofCoordinate) {
    switch (direction.toLowerCase()) {
      case 'north':
        return {
          latitude: ofCoordinate.latitude + 1,
          longitude: ofCoordinate.longitude
        }
      case 'northeast':
        return {
          latitude: ofCoordinate.latitude + 1,
          longitude: ofCoordinate.longitude - 1
        }
      case 'east':
        return {
          latitude: ofCoordinate.latitude,
          longitude: ofCoordinate.longitude - 1
        }
      case 'southeast':
        return {
          latitude: ofCoordinate.latitude - 1,
          longitude: ofCoordinate.longitude - 1
        }
      case 'south':
        return {
          latitude: ofCoordinate.latitude - 1,
          longitude: ofCoordinate.longitude
        }
      case 'southwest':
        return {
          latitude: ofCoordinate.latitude - 1,
          longitude: ofCoordinate.longitude + 1
        }
      case 'west':
        return {
          latitude: ofCoordinate.latitude,
          longitude: ofCoordinate.longitude + 1
        }
      default:
        return ofCoordinate;
    }
  };

  this.getCommonCollections = function () {
    return [
      'facilities',
      'features',
      'observations',
      'reports',
      'routes',
      'signs',
      'tracks',
      'trailheads'
    ];
  }

  this.login = function(username, password) {
    return chakram.post(_this.getBaseUrl() + '/members/login', {
      username: username,
      password: password
    });
  };

  this.logout = function(accessToken) {
    return chakram.post(_this.getBaseUrl + '/members/logout?access_token=' + accessToken);
  };

  this.delete = function(collection, id) {
    return _this.login('admin', 'password').then(function (loginResponse) {
      return chakram.delete(_this.getBaseUrl() + '/' + collection + '/' + id + '?access_token=' + loginResponse.body.id);
    });
  };

  this.create = function(username, password, collection, data) {
    return _this.login(username, password).then(function (loginResponse) {
      return chakram.post(_this.getBaseUrl() + '/' + collection + '?access_token=' + loginResponse.body.id, data);
    });
  };

};

module.exports = new helper();
