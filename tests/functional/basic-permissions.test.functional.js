'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('Basic Permissions', function () {

  describe('GET of collections is allowed for members not logged in', function () {

    it('GET to /aggregate/reports,trailheads should work', function () {
      return chakram.get(helper.getBaseUrl() + '/aggregate/reports,trailheads').then(function (response) {
        expect(response).to.have.status(200);
      });
    });

    it('GET to /aggregate/reports,trailheads/within should work', function () {
      var sw = helper.getValidCoordinate();
      var ne = helper.getCoordinateAt('northeast', sw);
      var params = '?southWestLatitude=' + sw.latitude + '&southWestLongitude=' + sw.longitude +
                   '&northEastLatitude=' + ne.latitude + '&northEastLongitude=' + ne.longitude;
      return chakram.get(helper.getBaseUrl() + '/aggregate/reports,trailheads/within' + params).then(function (response) {
        expect(response).to.have.status(200);
      });
    });

    it('GET to /aggregate/reports,trailheads should work', function () {
      return chakram.get(helper.getBaseUrl() + '/aggregate/reports,trailheads').then(function (response) {
        expect(response).to.have.status(200);
      });
    });

    it('GET to /members should work', function () {
      return chakram.get(helper.getBaseUrl() + '/members').then(function (response) {
        expect(response).to.have.status(200);
      });
    });

    helper.getCommonCollections().forEach(function (collection) {
      it('GET to /' + collection + ' should work', function () {
        return chakram.get(helper.getBaseUrl() + '/' + collection).then(function (response) {
          expect(response).to.have.status(200);
        });
      });
    });

  });

  describe('POST to common collections is not allowed for members not logged in', function () {
    helper.getCommonCollections().forEach(function (collection) {
      it('POST to /' + collection + ' should return unauthorized', function () {
        return chakram.post(helper.getBaseUrl() + '/' + collection, {}).then(function (response) {
          expect(response).to.have.status(401);
        });
      });
    });
  });

});
