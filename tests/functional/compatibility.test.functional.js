'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('Backwards Compatibility tests', function () {

  it('Ensure names show up in /routes call with fields filter', function () {

    var deletes = [];
    return helper.register({
        username: "testuser",
        email: "testuser@traildock.com",
        firstName: "testuser_firstname",
        lastName: "testuser_lastname",
        password: "password",
        passwordConfirm: "password"
      }, true).then(function (userResponse) {
        deletes.push({ collection: 'members', id: userResponse.body.id });
        return helper.create(userResponse.body.username, 'password', 'routes',
                      { name: 'created by ' + userResponse.body.username }).then(function (createResponse) {
          deletes.push({ collection: 'routes', id: createResponse.body.id });
          expect(createResponse.body.name).to.equal('created by ' + userResponse.body.username);
        });
      }).then(function () {
        return chakram.get(helper.getBaseUrl() + '/routes?filter={"fields":{"geometry":false}}');
      }).then(function (collectionResponse) {
        var promises = [];
        expect(collectionResponse.body[0].name).to.equal('created by ' + collectionResponse.body[0].createdByUsername);
        for (var i = 0; i < deletes.length; i++) {
          promises.push(helper.delete(deletes[i].collection, deletes[i].id));
        }
        return chakram.all(promises);
      });

  });

  it('Ensure names show up in /trailheads call with fields filter', function () {

    var deletes = [];
    return helper.register({
        username: "testuser",
        email: "testuser@traildock.com",
        firstName: "testuser_firstname",
        lastName: "testuser_lastname",
        password: "password",
        passwordConfirm: "password"
      }, true).then(function (userResponse) {
        deletes.push({ collection: 'members', id: userResponse.body.id });
        return helper.create(userResponse.body.username, 'password', 'trailheads',
                      { name: 'created by ' + userResponse.body.username }).then(function (createResponse) {
          deletes.push({ collection: 'trailheads', id: createResponse.body.id });
          expect(createResponse.body.name).to.equal('created by ' + userResponse.body.username);
        });
      }).then(function () {
        return chakram.get(helper.getBaseUrl() + '/trailheads?filter={"fields":{"geometry":false}}');
      }).then(function (collectionResponse) {
        var promises = [];
        expect(collectionResponse.body[0].name).to.equal('created by ' + collectionResponse.body[0].createdByUsername);
        for (var i = 0; i < deletes.length; i++) {
          promises.push(helper.delete(deletes[i].collection, deletes[i].id));
        }
        return chakram.all(promises);
      });

  });

  it('Ensure names show up in /tracks call with fields filter', function () {

    var deletes = [];
    return helper.register({
        username: "testuser",
        email: "testuser@traildock.com",
        firstName: "testuser_firstname",
        lastName: "testuser_lastname",
        password: "password",
        passwordConfirm: "password"
      }, true).then(function (userResponse) {
        deletes.push({ collection: 'members', id: userResponse.body.id });
        return helper.create(userResponse.body.username, 'password', 'tracks',
                      { name: 'created by ' + userResponse.body.username }).then(function (createResponse) {
          deletes.push({ collection: 'tracks', id: createResponse.body.id });
          expect(createResponse.body.name).to.equal('created by ' + userResponse.body.username);
        });
      }).then(function () {
        return chakram.get(helper.getBaseUrl() + '/tracks?filter={"fields":{"geometry":false}}');
      }).then(function (collectionResponse) {
        var promises = [];
        expect(collectionResponse.body[0].name).to.equal('created by ' + collectionResponse.body[0].createdByUsername);
        for (var i = 0; i < deletes.length; i++) {
          promises.push(helper.delete(deletes[i].collection, deletes[i].id));
        }
        return chakram.all(promises);
      });

  });

  it('Ensure headings show up in /reports call with fields filter', function () {

    var deletes = [];
    return helper.register({
        username: "testuser",
        email: "testuser@traildock.com",
        firstName: "testuser_firstname",
        lastName: "testuser_lastname",
        password: "password",
        passwordConfirm: "password"
      }, true).then(function (userResponse) {
        deletes.push({ collection: 'members', id: userResponse.body.id });
        return helper.create(userResponse.body.username, 'password', 'reports',
                      { heading: 'created by ' + userResponse.body.username }).then(function (createResponse) {
          deletes.push({ collection: 'reports', id: createResponse.body.id });
          expect(createResponse.body.heading).to.equal('created by ' + userResponse.body.username);
        });
      }).then(function () {
        return chakram.get(helper.getBaseUrl() + '/reports?filter={"fields":{"geometry":false}}');
      }).then(function (collectionResponse) {
        var promises = [];
        expect(collectionResponse.body[0].heading).to.equal('created by ' + collectionResponse.body[0].createdByUsername);
        for (var i = 0; i < deletes.length; i++) {
          promises.push(helper.delete(deletes[i].collection, deletes[i].id));
        }
        return chakram.all(promises);
      });

  });

});
