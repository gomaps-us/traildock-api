'use strict';

var helper  = require('./helper');
var chakram = require('chakram'),
    expect  = chakram.expect;

describe('Common Model Tests', function () {

  it('A registered user should be able to create common objects with a title, description', function () {

    var id;
    var ids = [];
    return helper.register({
      email: "testuser@traildock.com",
      firstName: "testuser_firstname",
      lastName: "testuser_lastname",
      password: "password",
      passwordConfirm: "password",
    }, true).then(function (userResponse) {
      id = userResponse.body.id;
      var promises = [];
      helper.getCommonCollections().forEach(function (collection) {
        var promise = helper.create(userResponse.body.username, 'password', collection, {
          title: 'common-model-test-title-' + collection,
          description: 'a description for common-model-test-title-' + collection,
        }).then(function (response) {
          ids.push({ collection: collection, id: response.body.id });
          expect(response).to.have.status(200);
          expect(response.body.title).to.not.be.empty;
          expect(response.body.description).to.not.be.empty;
        });
        promises.push(promise);
      });
      return chakram.all(promises).then(function () {
        return helper.delete('members', id);
      }).then(function () {
        var deletes = [];
        ids.forEach(function (id) {
          deletes.push(helper.delete(id.collection, id.id));
        });
        return chakram.all(deletes);
      });
    });

  });

});
