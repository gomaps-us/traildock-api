'use strict';

var chai      = require('chai');
var expect    = chai.expect;
var assert    = chai.assert;
var GoogleLib = require('../lib/google');
var config    = require('config');

describe('lib/google', function () {

  var google;

  before(function (done) {
    google = new GoogleLib(config.get('google.api.uri'), config.get('google.api.key'));
    done();
  });

  it('getElevation() should work for a point', function (done) {
    google.getElevation({
      type: 'Point',
      coordinates: [-106.03660583496094, 39.61600341690947]
    }).then(function (withElevation) {
      expect(withElevation).to.deep.equal([-106.0366058349609, 39.61600341690947, 2812.6572265625]);
      done();
    }).catch(function (error) {
      done(error);
    });
  });

  it('getElevation() should work for a LineString', function (done) {
    google.getElevation({
      type: 'LineString',
      coordinates: [[-106.03660583496094, 39.61600341690947], [-106.04201316833498, 39.62929193911971]]
    }).then(function (withElevation) {
      expect(withElevation).to.deep.equal([
        [-106.0366058349609, 39.61600341690947, 2812.6572265625], [-106.042013168335, 39.62929193911971, 2761.290283203125]
      ]);
      done();
    }).catch(function (error) {
      done(error);
    });
  });

  it('getElevation() should work for a MultiLineString', function (done) {
    google.getElevation({
      type: 'MultiLineString',
      coordinates: [
        [
          [-106.03660583496094, 39.61600341690947], [-106.04201316833498, 39.62929193911971]
        ], [
          [-106.03660583496094, 39.61600341690947], [-106.04201316833498, 39.62929193911971]
        ]
      ]
    }).then(function (withElevation) {
      expect(withElevation).to.deep.equal([
        [ -106.0366058349609, 39.61600341690947, 2812.6572265625 ],[ -106.042013168335, 39.62929193911971, 2761.290283203125 ],
        [ -106.0366058349609, 39.61600341690947, 2812.6572265625 ],[ -106.042013168335, 39.62929193911971, 2761.290283203125 ]
      ]);
      done();
    }).catch(function (error) {
      done(error);
    });
  });

});
