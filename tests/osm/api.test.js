'use strict';

var chai    = require('chai');
var expect  = chai.expect;
var assert  = chai.assert;
var config  = require('config');
var osm     = require('../../lib/osm')(config.osm);

describe('lib/osm/api', function () {

  it('initData() should return the correct structure', function (done) {
    var data = osm.api.initData('node');
    expect(data.osm.length).to.equal(1);
    expect(data.osm[0].node[0]._attr).to.be.empty;
    done();
  });

  it('createNode(), getNode(), updateNode(), then deleteNode() should work', function (done) {
    var id;
    osm.api.createNode(38.77469603001716, -106.82135581970216, [ { key: 'one', value: 1 }, { key: 'two', value: 1 }]).then(function (response) {
      expect(response.statusCode).to.equal(200);
      expect(response.body).to.be.above(0);
      id = response.body;
      return osm.api.getNode(response.body);
    }).then(function (response) {
      expect(response.body.osm.node[0].$.id).to.be.above(0);
      return osm.api.updateNode(id, 43.9820000, -105.90, [ { key: 'one', value: 'UPDATED' }]);
    }).then(function (response) {
      return osm.api.getNode(id);
    }).then(function (response) {
      expect(response.body.osm.node[0].$.lat).to.equal('43.9820000');
      return osm.api.deleteNode(id);
    }).then(function (response) {
      expect(response.body).to.equal('3');
      done();
    }).catch(function (error) {
      done(error);
    });
  });

});
