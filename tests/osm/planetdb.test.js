'use strict';

var chai    = require('chai');
var expect  = chai.expect;
var assert  = chai.assert;
var config  = require('config');
var osm     = require('../../lib/osm')(config.osm);
var geojson = require('../../lib/geojson');

describe.skip('lib/osm/planetdb', function () {

  var planetdb  = null;
  var ds        = null;

  before(function (done) {
    var DataSource = require('loopback-datasource-juggler').DataSource;
    ds = new DataSource({
      connector: require('loopback-connector-postgresql'),
      user: config.get('osm.planetdb.username'),
      password: config.get('osm.planetdb.password'),
      database: config.get('osm.planetdb.db'),
      host: config.get('osm.planetdb.host'),
      port: config.get('osm.planetdb.port'),
    });
    ds.on('connected', function () {
      planetdb = new osm.planetdb(ds.connector);
      done();
    });
  });

  it('query() should generally work', function (done) {
    planetdb.query('SELECT * from planet_osm_nodes LIMIT $1', [1]).then(function (result) {
      expect(result).to.have.lengthOf(1);
      done();
    });
  });

  it('getGeoJson should return a Point GeoJson structure for a known node', function (done) {
    planetdb.getGeoJson({ source: 'osm', type: 'node', sid: 25689366 }).then(function (json) {
      expect(geojson.isPoint(json.coordinates)).to.equal(true);
      expect(json.type).to.equal('Point');
      done();
    });
  });

  it('getGeoJson should return a LineString GeoJson structure for a known way', function (done) {
    planetdb.getGeoJson({ source: 'osm', type: 'way', sid: 119879879 }).then(function (json) {
      expect(geojson.isLineString(json.coordinates)).to.equal(true);
      expect(json.type).to.equal('LineString');
      done();
    });
  });

  it('getGeoJson should return a LineString GeoJson structure for a known relation', function (done) {
    planetdb.getGeoJson({ source: 'osm', type: 'relation', sid: 7281887 }).then(function (json) {
      expect(geojson.isLineString(json.coordinates)).to.equal(true);
      expect(json.type).to.equal('LineString');
      done();
    });
  });

  after(function (done) {
    ds.disconnect();
    done();
  });

});
