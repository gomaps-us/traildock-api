'use strict';

var chai    = require('chai');
var expect  = chai.expect;
var assert  = chai.assert;
var config  = require('config');
var osm     = require('../../lib/osm')(config.osm);

describe('lib/osm/index', function () {

  it('ensure getTagValue() works', function (done) {
    expect(osm.getTagValue('whatever', [])).to.be.null;
    expect(osm.getTagValue('one', [ { key: 'one', value: 1 }])).to.equal(1);
    done();
  });

  it('ensure setTagValue() sets a new value', function (done) {
    var tags = [{ key: 'one', value: 1 }];
    osm.setTagValue('two', 2, tags);
    expect(tags.length).to.equal(2);
    expect(tags[1].key).to.equal('two');
    expect(tags[1].value).to.equal(2);
    done();
  });

  it('ensure setTagValue() replaces an existing key/value', function (done) {
    var tags = [{ key: 'one', value: 1 }, { key: 'two', value: 2 }, { key: 'three', value: 3 }];
    osm.setTagValue('two', 10, tags);
    expect(tags.length).to.equal(3);
    expect(tags[1].key).to.equal('two');
    expect(tags[1].value).to.equal(10);
    done();
  });

  it('makeOsmTags() should return the correct tags for a general facility', function (done) {
    var modelName = 'facility';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-facility',
      description: 'test-facility description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('trail_facility');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-facility');
    expect(osm.getTagValue('description', tags)).to.equal('test-facility description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a general feature', function (done) {
    var modelName = 'feature';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-feature',
      description: 'test-feature description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('natural', tags)).to.equal('feature');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-feature');
    expect(osm.getTagValue('description', tags)).to.equal('test-feature description');
    done();
  });

  it('makeOsmTags() should return the correct tags for an observation with no title', function (done) {
    var modelName = 'observation';
    var data = {
      createdByUsername: 'testuser',
      description: 'test-observation description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('observation', tags)).to.equal('trail');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('');
    expect(osm.getTagValue('description', tags)).to.equal('test-observation description');
    done();
  });

  it('makeOsmTags() should return the correct tags for an observation with a blank title', function (done) {
    var modelName = 'observation';
    var data = {
      createdByUsername: 'testuser',
      description: 'test-observation description',
      title: '',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('observation', tags)).to.equal('trail');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('');
    expect(osm.getTagValue('description', tags)).to.equal('test-observation description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a report', function (done) {
    var modelName = 'report';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-report',
      description: 'test-report description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('tourism', tags)).to.equal('information');
    expect(osm.getTagValue('information', tags)).to.equal('trail_report');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-report');
    expect(osm.getTagValue('description', tags)).to.equal('test-report description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a sign', function (done) {
    var modelName = 'sign';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-sign',
      description: 'test-sign description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('tourism', tags)).to.equal('information');
    expect(osm.getTagValue('information', tags)).to.equal('board');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-sign');
    expect(osm.getTagValue('description', tags)).to.equal('test-sign description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a trailhead', function (done) {
    var modelName = 'trailhead';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-trailhead',
      description: 'test-trailhead description',
      tags: []
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('trailhead');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-trailhead');
    expect(osm.getTagValue('description', tags)).to.equal('test-trailhead description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a facility subtype:campground', function (done) {
    var modelName = 'facility';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-facility',
      description: 'test-facility description',
      tags: [
        'subtype:campground'
      ]
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('trail_facility');
    expect(osm.getTagValue('tourism', tags)).to.equal('camp_site');
    expect(osm.getTagValue('backcountry', tags)).to.equal('yes');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-facility');
    expect(osm.getTagValue('description', tags)).to.equal('test-facility description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a facility subtype:toilet', function (done) {
    var modelName = 'facility';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-facility',
      description: 'test-facility description',
      tags: [
        'subtype:toilet'
      ]
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('toilets');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-facility');
    expect(osm.getTagValue('description', tags)).to.equal('test-facility description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a facility subtype:shelter', function (done) {
    var modelName = 'facility';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-facility',
      description: 'test-facility description',
      tags: [
        'subtype:shelter'
      ]
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('shelter');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-facility');
    expect(osm.getTagValue('description', tags)).to.equal('test-facility description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a feature subtype:water', function (done) {
    var modelName = 'feature';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-feature',
      description: 'test-feature description',
      tags: [
        'subtype:water'
      ]
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('amenity', tags)).to.equal('drinking_water');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-feature');
    expect(osm.getTagValue('description', tags)).to.equal('test-feature description');
    done();
  });

  it('makeOsmTags() should return the correct tags for a sign subtype:trailpost', function (done) {
    var modelName = 'sign';
    var data = {
      createdByUsername: 'testuser',
      title: 'test-sign',
      description: 'test-sign description',
      tags: [
        'subtype:trailpost'
      ]
    };
    var tags = osm.makeOsmTags(modelName, data);
    expect(osm.getTagValue('tourism', tags)).to.equal('information');
    expect(osm.getTagValue('information', tags)).to.equal('guidepost');
    expect(osm.getTagValue('traildock:user', tags)).to.equal('testuser');
    expect(osm.getTagValue('name', tags)).to.equal('test-sign');
    expect(osm.getTagValue('description', tags)).to.equal('test-sign description');
    done();
  });

});
