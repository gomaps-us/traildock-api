'use strict';

var config = require('config');

module.exports = function (app) {

  var Member = app.models.member;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  // Because of this: https://github.com/strongloop/loopback-connector-mongodb/issues/128
  var ObjectID = RoleMapping.getDataSource().connector.getDefaultIdType();
  RoleMapping.defineProperty('principalId', {
    type: ObjectID,
  });

  var adminAttributes = {
    id: null,
    firstName: 'Traildock',
    lastName: 'Admin',
    username: 'admin',
    email: config.get('admin.email'),
    password: config.get('admin.password'),
    emailVerified: true,
  };

  Member.find({ where: { username: 'admin' }, limit: 1 }, function (err, existing) {
    if (err) throw err;
    if (existing.length > 0) {
      adminAttributes.id = existing[0].id;
      existing[0].updateAttributes(adminAttributes, function (err, updated) {
        console.log('Updated admin user:', updated);
      });
      return;
    }

    Member.create(adminAttributes, function (err, member) {
      if (err) throw err;
      console.log('Created admin user:', member);
      Role.find({ where: { name: 'admin' }, limit: 1 }, function (err, roles) {
        if (err) throw err;
        if (roles.length > 0) return;
        Role.create({
          name: 'admin',
        }, function (err, role) {
          if (err) throw err;
          console.log('Created admin role:', role);
          role.principals.create({
            principalType: RoleMapping.USER,
            principalId: member.id,
          }, function (err, principal) {
            if (err) throw err;
            console.log('Created admin role principal:', principal);
          });
        });

      });

    });

  });

};
