'use strict';

var path    = require('path');
var config  = require('config');

module.exports = function (app) {

  app.get('/', app.loopback.status());

  app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    if (config.get('robots') !== true) {
      res.send('User-agent: *\nDisallow: /');
    } else {
      res.send('User-agent: *\nDisallow: /\nAllow: /docs');
    }
  });

  app.get('/password-reset', function (req, res) {
    res.redirect(config.get('traildock.com.uri') + '/forgot');
  });

};
