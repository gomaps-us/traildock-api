'use strict';

var loopback    = require('loopback');
var boot        = require('loopback-boot');
var consolidate = require('consolidate');
var path        = require('path');
var explorer    = require('loopback-component-explorer');
var ops         = require('traildock-node-library').ops;

ops.crypt(path.resolve(__dirname, '..')).decryptConfigs(true);

var app = module.exports = loopback();

app.engine('pug', consolidate.pug);
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.use(loopback.static(path.join(path.resolve(__dirname, '..'), 'client')));

app.use(function (req, res, next) {
  app.set('host', req.headers.host);
  app.set('secure', req.secure);
  app.set('serverDir', __dirname);
  next();
});

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    explorer(app, {
      mountPath: '/docs',
      apiInfo: app.get('docs'),
      uiDirs: path.resolve(__dirname, 'explorer'),
    });
    console.log('Browse your REST API at %s%s', baseUrl, app.get('loopback-component-explorer').mountPath);
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  app.dataSources.mongodb.autoupdate();

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
