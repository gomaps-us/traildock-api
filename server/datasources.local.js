'use strict';

var config        = require('config');
var child_process = require('child_process');

var makeMongoConnectionString = function(user, password, hosts, db, ssl, replicaSet, authSource) {
  var connection = 'mongodb://';
  if (user && password) {
    connection += user + ':' + password + '@';
  }
  connection += hosts.map(function (host) {
    if (host.name == 'docker-machine' || host.name == 'mongodb') {
      var original = host.name;
      try {
        host.name = child_process.execSync('docker-machine ip').toString().trim();
      } catch (e) {
        host.name = original;
      }
    }
    return host.name + ':' + host.port;
  }).join(',');
  connection += '/' + db;
  var params = '';
  if (ssl === true) {
    params += '?ssl=true';
  }
  if (replicaSet) {
    params += (params == '' ? '?' : '&') + 'replicaSet=' + replicaSet;
  }
  if (authSource) {
    params += (params == '' ? '?' : '&') + 'authSource=' + authSource;
  }
  connection += params;
  return connection;
};

module.exports = {
  mongodb: {
    name: 'mongodb',
    url: makeMongoConnectionString(
      config.get('mongo.username'),
      config.get('mongo.password'),
      config.get('mongo.hosts'),
      config.get('mongo.db'),
      config.get('mongo.ssl'),
      config.get('mongo.replicaSet'),
      config.get('mongo.authSource')
    ),
    connector: 'mongodb',
    enableGeoIndexing: true
  },
  emailDs: {
    name: 'emailDs',
    connector: 'mail',
    transports: [{
      type: 'SMTP',
      host: config.get('smtp.host'),
      secure: config.get('smtp.secure'),
      port: config.get('smtp.port'),
      auth: {
        user: config.get('smtp.user'),
        pass: config.get('smtp.password')
      }
    }]
  },
  // TODO: we no longer have a running planetdb, so we can't use this functionality until we add it back
  // osmPlanetdb: {
  //   name: 'osm',
  //   connector: 'postgresql',
  //   host: config.get('osm.planetdb.host'),
  //   port: config.get('osm.planetdb.port'),
  //   username: config.get('osm.planetdb.username'),
  //   password: config.get('osm.planetdb.password'),
  //   database: config.get('osm.planetdb.db')
  // },
};
