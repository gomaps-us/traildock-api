$(document).ready(function() {
  $('#logo').replaceWith('<img class="traildock-logo" src="https://cdn.traildock.com/images/traildock-text.png" /><span class="api">API</span>');
  $('html title').text('Traildock API Docs');
});
