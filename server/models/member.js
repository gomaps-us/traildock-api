'use strict';

var fs              = require('fs');
var pug             = require('pug');
var gravatar        = require('gravatar');
var shared          = require('../../lib/shared');
var enabledMethods  = [
  'create', 'exists', 'findById', 'find', 'findOne', 'deleteById', 'count',
  'login', 'logout', 'resetPassword', 'updatePassword', 'confirm', 'verifyEmail', 'me', 'updatePartial',
];
var randomstring    = require('randomstring');
var config          = require('config');
var _               = require('underscore');
var compatibility   = require('../../lib/compatibility');
var crypto          = require('crypto');
var debug           = require('debug')('loopback:user');

module.exports = function (Member) {

  shared.disableMethods(Member, enabledMethods);
  shared.addCommonRemoteMethods(Member);

  Member.observe('before save', function (context, next) {
    if (context.instance && !context.instance.avatarUrl) {
      context.instance.avatarUrl = gravatar.url(context.instance.email, { d: 'mm' });
    }
    next();
  });

  Member.beforeRemote('create', function (context, instance, next) {
    if (context.req.query.privilegedToken == config.get('traildock.tokens.priv') && context.req.body.forceEmailVerified == true) {
      Member.settings.emailVerificationRequired = false;
      instance.emailVerified = true;
      context.req.body.emailVerified = true;
      delete context.req.body.forceEmailVerified;
    } else {
      context.req.body.emailVerified = false;
      instance.emailVerified = false;
    }
    var error = new Error();
    if (!context.req.body.firstName || !context.req.body.lastName) {
      error.status = 422;
      error.message = 'first and last name are required (firstName, lastName)';
      return next(error);
    }
    if (!context.req.body.username || context.req.body.username.trim() == '') {
      context.req.body.username = context.req.body.email;
    }
    if (!context.req.body.passwordConfirm) {
      error.status = 422;
      error.message = 'please include a passwordConfirm field so we can verify the value of your new password';
      return next(error);
    }
    if (context.req.body.password != context.req.body.passwordConfirm) {
      error.status = 422;
      error.message = 'the password and passwordConfirm values do not match';
      return next(error);
    }
    delete context.req.body.passwordConfirm;
    next();
  });

  Member.afterRemote('create', function (context, instance, next) {
    Member.sendVerificationEmail(instance, next);
  });

  Member.verifyEmail = function (email, cb) {
    Member.findOne({ where: { email: email } }, function (err, member) {
      if (err) return cb(err);
      var error = new Error();
      error.status = 404;
      error.message = 'No member found with email ' + email;
      if (!member || !member.email) return cb(error);
      Member.sendVerificationEmail(member, cb);
    });
  };

  Member.sendVerificationEmail = function (member, cb) {
    var options = {
      type: 'email',
      to: member.email,
      from: 'Traildock <postmaster@traildock.com>',
      subject: 'Welcome to Traildock!',
      template: member.emailVerified ? Member.app.get('serverDir') + '/views/email/welcome.pug' : Member.app.get('serverDir') + '/views/email/verify.pug',
      redirect: config.get('traildock.com.uri') + '/verified?name=' + member.firstName,
      traildockUrl: config.get('traildock.com.uri'),
      templateFn: function (verifyOptions, options, cb) {
        var template = pug.compileFile(verifyOptions.template);
        var body = template(verifyOptions);
        cb(null, body);
      },
      protocol: 'http',
      host: Member.app.get('host').split(':')[0],
      port: 80,
      user: member,
      baseUrl: (Member.app.get('secure') ? 'https' : 'http') + '://' + Member.app.get('host'),
    };

    member.verify(options, function (err, response) {
      if (err) {
        Member.deleteById(member.id);
        return cb(err);
      }
      cb();
    });
  };

  Member.getMemberByAccessToken = function (accessToken, cb) {
    Member.relations.accessTokens.modelTo.findById(accessToken, function (err, accessToken) {
      if (err) return cb(err);
      err             = new Error();
      err.statusCode  = 404;
      err.message     = 'Access token is not valid';
      if (!accessToken) return cb(err);
      Member.findById(accessToken.userId, function (err, member) {
        if (err) return cb(err);
        cb(null, member);
      });
    });
  };

  Member.completeResetPassword = function (id, accessToken, data, cb) {
    var err;
    var passwordData = compatibility.getStandardizedPasswordProperties(data);

    //verify passwords match
    if (!passwordData.password || !passwordData.passwordConfirm || passwordData.password !== passwordData.passwordConfirm) {
      err = new Error('Passwords do not match');
      err.statusCode = 422;
      return cb(err);
    }

    Member.getMemberByAccessToken(accessToken, function (err, member) {
      if (err) return cb(err);
      member.updateAttribute('password', passwordData.password, function (err, memberUpdated) {
        if (err) return cb(err);
        cb(null, {
          message: 'Your password has been reset successfully',
        });
      });
    });

  };

  Member.me = function (accessToken, cb) {
    Member.findById(accessToken.userId, function (err, member) {
      if (err) return cb(err);
      cb(null, member);
    });
  };

  Member.updatePassword = function (userId, data, options, cb) {
    var passwordData = compatibility.getStandardizedPasswordProperties(data);
    Member.findById(userId, function (err, instance) {
      if (err) return cb(err);
      if (!passwordData.oldPassword || !passwordData.password || !passwordData.passwordConfirm) {
        err = new Error('oldPassword, newPassword, and confirmPassword must be present in the posted data');
        err.statusCode = 422;
        return cb(err);
      }
      if (passwordData.password != passwordData.passwordConfirm) {
        err = new Error('New password and password confirmation don\'t match');
        err.statusCode = 422;
        return cb(err);
      }
      if (!instance) {
        err = new Error('User ' + userId + ' not found');
        Object.assign(err, {
          code: 'USER_NOT_FOUND',
          statusCode: 404,
        });
        return cb(err);
      }
      instance.changePassword(passwordData.oldPassword, passwordData.password, options, cb);
    });

    return cb.promise;
  };

  Member.autoLogin = function (data, options, cb) {
    var err;
    if (data.privilegedToken != config.get('traildock.tokens.priv')) {
      err = new Error('Forbidden');
      err.statusCode = 403;
      return cb(err);
    }
    Member.findById(data.id, function (err, member) {
      if (err) return cb(err);
      if (!member) {
        err = new Error('Not Found');
        err.statusCode = 404;
        return cb(err);
      }
      crypto.randomBytes(24, function(err, buffer) {
        // autoLogin is a reserved resource for implementing oauth capabilities, we will randomly set a secure password
        // for the local user, but this password should never be needed for these types of users
        var newPassword = buffer.toString('hex');
        member.setPassword(newPassword, function (err) {
          if (err) return cb(err);
          Member.login({ username: member.username, password: newPassword }, function (err, accessToken) {
            if (err) return cb(err);
            cb(null, accessToken);
          });
        });
      });
    });
  };

   Member.getMemberRoles = function (id, cb) {
    Member.getApp(function (err, app) {
      if (err) throw err;
      var RoleMapping = app.models.RoleMapping;
      var Role = app.models.Role;
      RoleMapping.find({ where : { principalId: id } }, function (err, mapping) {
        if (mapping.length == 0) return cb(null, []);
        var roleIds = _.uniq(mapping.map(function (mapping) {
          return mapping.roleId;
        }));
        var conditions = roleIds.map(function (roleId) {
          return { id: roleId };
        });
        Role.find({ where: { or: conditions } }, function (err, roles) {
          if (err) throw err;
          var roleNames = roles.map(function(role) {
            return role.name;
          });
          cb(null, roleNames);
        });
      });
    });
  };

  Member.on('resetPasswordRequest', function (info) {
    var emailTemplate = pug.compileFile(Member.app.get('serverDir') + '/views/email/password-reset.pug');

    var html = emailTemplate({
      baseUrl: config.get('traildock.com.uri'),
      path: '/reset?id=' + info.accessToken.userId + '&access_token=' + info.accessToken.id,
    });

    Member.app.models.Email.send({
      to: info.email,
      from: 'Traildock <postmaster@traildock.com>',
      subject: 'Password Reset Request',
      html: html,
    }, function (err) {
      if (err) console.error(err);
    });
  });

  Member.remoteMethod('create', shared.getCreateDefinition(Member));

/**********************************************************************************
COPIED OVER FROM ROOT LOOPBACK, TODO: maybe there's a better way to extend here
*/
  Member.login = function(credentials, include, fn) {
    var self = this;
    if (typeof include === 'function') {
      fn = include;
      include = undefined;
    }

    fn = fn || utils.createPromiseCallback();

    include = (include || '');
    if (Array.isArray(include)) {
      include = include.map(function(val) {
        return val.toLowerCase();
      });
    } else {
      include = include.toLowerCase();
    }

    var realmDelimiter;
    // Check if realm is required
    var realmRequired = !!(self.settings.realmRequired ||
      self.settings.realmDelimiter);
    if (realmRequired) {
      realmDelimiter = self.settings.realmDelimiter;
    }
    var query = self.normalizeCredentials(credentials, realmRequired,
      realmDelimiter);

    if (realmRequired && !query.and[1].realm) {
      var err1 = new Error('realm is required');
      err1.statusCode = 400;
      err1.code = 'REALM_REQUIRED';
      fn(err1);
      return fn.promise;
    }
    if (!query.and[0].length == 0) {
      var err2 = new Error('username or email is required');
      err2.statusCode = 400;
      err2.code = 'USERNAME_EMAIL_REQUIRED';
      fn(err2);
      return fn.promise;
    }

    self.findOne({ where: query }, function(err, user) {
      var defaultError = new Error('login failed');
      defaultError.statusCode = 401;
      defaultError.code = 'LOGIN_FAILED';

      function tokenHandler(err, token) {
        if (err) return fn(err);
        if (Array.isArray(include) ? include.indexOf('user') !== -1 : include === 'user') {
          // NOTE(bajtos) We can't set token.user here:
          //  1. token.user already exists, it's a function injected by
          //     "AccessToken belongsTo User" relation
          //  2. ModelBaseClass.toJSON() ignores own properties, thus
          //     the value won't be included in the HTTP response
          // See also loopback#161 and loopback#162
          token.__data.user = user;
        }
        fn(err, token);
      }

      if (err) {
        debug('An error is reported from User.findOne: %j', err);
        fn(defaultError);
      } else if (user) {
        user.hasPassword(credentials.password, function(err, isMatch) {
          if (err) {
            debug('An error is reported from User.hasPassword: %j', err);
            fn(defaultError);
          } else if (isMatch) {
            if (self.settings.emailVerificationRequired && !user.emailVerified) {
              // Fail to log in if email verification is not done yet
              debug('User email has not been verified');
              err = new Error('login failed as the email has not been verified');
              err.statusCode = 401;
              err.code = 'LOGIN_FAILED_EMAIL_NOT_VERIFIED';
              err.details = {
                userId: user.id,
              };
              fn(err);
            } else {
              if (user.createAccessToken.length === 2) {
                user.createAccessToken(credentials.ttl, tokenHandler);
              } else {
                user.createAccessToken(credentials.ttl, credentials, tokenHandler);
              }
            }
          } else {
            debug('The password is invalid for user %s', query.email || query.username);
            fn(defaultError);
          }
        });
      } else {
        debug('No matching record is found for user %s', query.email || query.username);
        fn(defaultError);
      }
    });
    return fn.promise;
  };

  Member.normalizeCredentials = function(credentials, realmRequired, realmDelimiter) {
    var query = { "and": [{ "or": [] }] };
    credentials = credentials || {};
    if (!realmRequired) {
      if (credentials.email) {
        query.and[0].or.push({ "email": credentials.email });
      }
      if (credentials.username) {
        query.and[0].or.push({ "username": credentials.username });
      }
    } else {
      if (credentials.realm) {
        query.and.push({ "realm": credentials.realm });
      }
      var parts;
      if (credentials.email) {
        parts = splitPrincipal(credentials.email, realmDelimiter);
        query.and[0].or.push({ "email": parts[1] });
        if (parts[0]) {
          query.and.realm = parts[0];
        }
      } else if (credentials.username) {
        parts = splitPrincipal(credentials.username, realmDelimiter);
        query.and[0].or.push({ "username": parts[1] });
        if (parts[0]) {
          query.and.realm = parts[0];
        }
      }
    }

    return query;
  };

/**********************************************************************************
END COPIED OVER FROM ROOT LOOPBACK
*/

  Member.remoteMethod('completeResetPassword', {
    description: 'Completes the member password reset request',
    http: { verb: 'post', path: '/:id/reset' },
    accepts: [
      { arg: 'id', type: 'string', http: { source: 'path' }, required: true },
      { arg: 'access_token', type: 'string', http: { source: 'query' }, required: true },
      { arg: 'options', type: 'object', required: true, http: { source: 'body' } },
    ],
    returns: { type: 'object', root: true },
  });

  Member.remoteMethod('verifyEmail', {
    description: 'Triggers sending of account verification email',
    http: { verb: 'post', path: '/:email/verify' },
    accepts: [
      { arg: 'email', type: 'string', required: true },
    ],
    returns: { type: 'object', root: true },
  });

  Member.remoteMethod('me', {
    description: 'Gets the member record per the access token in the request',
    http: { verb: 'get', path: '/me' },
    accepts: [
      {
        arg: 'accessToken',
        type: 'object',
        http: function (ctx) {
          return ctx.req.accessToken;
        },
      },
    ],
    returns: { type: 'object', root: true },
  });

  Member.remoteMethod('updatePassword', {
    description: 'Update a member\'s password.',
    accepts: [
      {
        arg: 'id',
        type: 'any',
        http: function (ctx) {
          if (ctx.req.accessToken) return ctx.req.accessToken.userId;
        },
      },
      { arg: 'data', type: 'object', required: true, http: { source: 'body' } },
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    ],
    http: { verb: 'POST', path: '/update-password' },
  });

  Member.remoteMethod('autoLogin', {
    description: 'Auto login a user based on username, highly restricted',
    accepts: [
      { arg: 'data', type: 'object', required: true, http: { source: 'body' } },
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    ],
    http: { verb: 'POST', path: '/autoLogin' },
    returns: { type: 'object', root: true },
  });

  Member.remoteMethod('getMemberRoles', {
    http: { verb: 'get', path: '/:id/roles' },
    accepts: [
      { arg: 'id', type: 'string', http: { source: 'path' }, required: true },
    ],
    returns: { type: 'object', root: true },
  });

};
