'use strict';

var shared = require('../../lib/shared');

module.exports = function (Meta) {

  Meta._enabledMethods = ['findById', 'deleteById', 'upsertMeta', 'findMeta'];
  Meta.beforeRemote('findMeta', shared.beforeFind);

  Meta.findMeta = function (context, filter, source, type, sid, options, cb) {
    Meta.app.set('_options', options);
    if (!filter) filter = {};
    if (!filter.where) filter.where = {};
    var filterAnd = filter.where.and ? filter.where.and : null;
    filter.where = Object.assign(filter.where, {
      and: [
          { 'association.source': source },
          { 'association.type': type },
          { 'association.sid': sid },
      ],
    });
    if (filterAnd) {
      filter.where.and = filter.where.and.concat(filterAnd);
    }
    Meta.find(filter, function (err, meta) {
      if (err) return cb(err);
      if (meta.length == 0 && options.accessToken != null) {
        context.options._skipFind = true;
        Meta.upsertMeta(context, source, type, sid, {}, function (err, meta) {
          if (err) return cb(err);
          cb(null, [meta]);
        });
      } else {
        cb(null, meta);
      }
    });
  };

  Meta.upsertMeta = function (context, source, type, sid, data, options, cb) {
    Meta.app.set('_options', options);
    var isPublic = ((('public' in data) && context.req.accessToken != null) ? data.public : true);
    var where = {
      and: [
        { 'association.source': source },
        { 'association.type': type },
        { 'association.sid': sid },
        { 'public': isPublic },
      ],
    };
    if (!isPublic) {
      where.and.push({ 'createdBy': options.accessToken.userId });
    };
    var doSave = function (err, existing) {
      if (err) return cb(err);
      delete data['public'];
      for (var key in data) {
        if (data[key] === null || data[key] === undefined) {
          delete data[key];
          if (existing && existing.length && (key in existing[0].meta)) {
            delete existing[0].meta[key];
          }
        }
      }
      var upsert = {
        'association': {
          'source': source,
          'type': type,
          'sid': sid,
        },
        'public': isPublic,
        'meta': Object.assign(((existing && existing.length > 0) ? existing[0].meta : {}), data),
      };
      Meta.upsertWithWhere(where, upsert, cb);
    };
    if (context.options._skipFind) {
      doSave(null, []);
    } else {
      Meta.find({ where: where }, function (err, existing) {
        doSave(err, existing);
      });
    }
  };

  Meta.remoteMethod('findMeta', {
    description: 'Get all metadata for an object, path would be like `/meta/osm/node/123`',
    accessType: 'READ',
    http: {
      path: '/:source/:type/:sid',
      verb: 'get',
    },
    accepts: [
      { arg: 'context', type: 'object', http: { source: 'context' } },
      { arg: 'filter', type: 'object', description: 'Filter defining fields, where, include, order, offset, and limit' },
      { arg: 'source', type: 'string', required: true, description: 'The source of the object to which the metadata is attached, i.e. "osm"' },
      { arg: 'type', type: 'string', required: true, description: 'The type of object at the source' },
      { arg: 'sid', type: 'string', required: true, description: 'The ID of the source object to which the metadata is attached' },
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    ],
    returns: [
      { arg: 'data', type: ['meta'], root: true },
    ],
  });

  Meta.remoteMethod('upsertMeta', {
    description: 'Add metadata for an object, path would be like `/meta/osm/way/123`',
    accessType: 'WRITE',
    http: {
      path: '/:source/:type/:sid',
      verb: 'post',
    },
    accepts: [
      { arg: 'context', type: 'object', http: { source: 'context' } },
      { arg: 'source', type: 'string', required: true, description: 'The source of the object to which the metadata is attached, i.e. "osm"' },
      { arg: 'type', type: 'string', required: true, description: 'The type of object at the source' },
      { arg: 'sid', type: 'string', required: true, description: 'The source ID of the source object to which the metadata is attached' },
      { arg: 'data', type: 'object', http: { source: 'body' }, description: 'generic metadata to associate' },
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    ],
    returns: [
      { arg: 'data', type: 'meta', root: true },
    ],
  });

  Meta.remoteMethod('upsertMeta', {
    description: 'Update metadata for an object, path would be like `/meta/osm/relation/123`',
    accessType: 'WRITE',
    http: {
      path: '/:source/:type/:sid',
      verb: 'put',
    },
    accepts: [
      { arg: 'context', type: 'object', http: { source: 'context' } },
      { arg: 'source', type: 'string', required: true, description: 'The source of the object to which the metadata is attached, i.e. "osm"' },
      { arg: 'type', type: 'string', required: true, description: 'The type of object at the source' },
      { arg: 'sid', type: 'string', required: true, description: 'The source ID of the source object to which the metadata is attached' },
      { arg: 'data', type: 'object', http: { source: 'body' }, description: 'generic metadata to associate' },
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    ],
    returns: [
      { arg: 'data', type: 'meta', root: true },
    ],
  });

};
