'use strict';

var config  = require('config');
var osm     = require('../../lib/osm')(config.osm);
var Promise = require('bluebird');

module.exports = function (Report) {

  Report.setFromGeometry = function(Model, data) {
    return Promise.resolve(data);
    // TODO: figure out a way to re-enable this either by way of the api or another mirror of planet.osm
    // var planetdb = new osm.planetdb(Model.app.dataSources.osmPlanetdb.connector);
    // var lon = data.geometry.coordinates[0];
    // var lat = data.geometry.coordinates[1];
    // if (data.title && data.title != "") return Promise.resolve(data);
    // if (isNaN(lon) || isNaN(lat)) return Promise.resolve(data);
    // var query = "\
    //   SELECT name FROM planet_osm_line\
    //   WHERE highway = 'path'\
    //   AND ST_DistanceSphere(\
    //     ST_Transform(way,4326),\
    //     ST_GeomFromText('POINT(" + lon + " " + lat + ")',4326)\
    //   ) <= 15";
    // return planetdb.query(query).then(function (results) {
    //   var names = [];
    //   results.forEach(function (row) {
    //     names.push(row.name);
    //   });
    //   if (names.length > 0) {
    //     data.title = names.join(',');
    //   } else {
    //     data.title = "";
    //   }
    //   return data;
    // });
  }

}
