'use strict';

var config          = require('config');
var Promise         = require('bluebird');
var Forms           = require('formidable');
var stream          = require('stream');
var fs              = require('fs');
var dataUriToBuffer = require('data-uri-to-buffer');
var Aws             = require('aws-sdk');
Aws.config.update({ accessKeyId: config.get('aws.accessKeyId'), secretAccessKey: config.get('aws.secretAccessKey') });
var s3              = require('s3-upload-stream')(new Aws.S3());
var shortuuid       = require('short-uuid');
var mime            = require('mime-types');
var shared          = require('../../lib/shared');

module.exports = function (Files) {

  var form = new Forms({
    maxFileSize: config.get('files.maxSizeMb') * 1024 * 1024
  });

  Files.buildFileResponse = function (s3Response) {
    return {
      name: s3Response.Key.split('/').pop(),
      location: (config.get('aws.cloudfrontDomain') && config.get('aws.cloudfrontDomain') != '') ?
        'https://' + config.get('aws.cloudfrontDomain') + '/' + s3Response.Key :
        s3Response.Location
    };
  },

  Files.upload = function (context, cb) {
    var results = [];
    if (context._files && context._files.length > 0 && context._userId) {
      var toProcess = [];
      context._files.forEach(function (file) {
        if (file.match(/^data:/g) !== null) {
          toProcess.push(file);
        }
      });
      toProcess.forEach(function (file) {
        var read    = new stream.Readable();
        var buffer  = dataUriToBuffer(file);
        read.push(buffer);
        var upload    = s3.upload({
          Bucket: config.get('aws.s3Bucket'),
          Key: shared.makeS3FilePath(config.get('aws.s3Path'), context._userId, shortuuid.uuid() + '.' + mime.extension(buffer.type)),
          ACL: 'public-read',
          ContentType: buffer.type
        });
        read.push();
        read.push(null);
        upload.on('error', function (err) {
          return cb(err);
        });
        upload.on('uploaded', function(response) {
          results.push(Files.buildFileResponse(response));
          if (results.length == toProcess.length) {
            return cb(null, results);
          }
        });
        read.pipe(upload);
      });
      if (toProcess.length == 0) return cb(null, []);
    } else if (context.req) {
      form.parse(context.req, function(err, fields, files) {
        if (err) return cb(err);
        for (var key in files) {
          var file      = files[key];
          var read      = fs.createReadStream(file.path);
          var upload    = s3.upload({
            Bucket: config.get('aws.s3Bucket').split('/')[0],
            Key: shared.makeS3FilePath(config.get('aws.s3Path'), context.req.accessToken.userId, file.name),
            ACL: 'public-read',
            ContentType: file.type
          });
          upload.on('error', function (err) {
            return cb(err);
          });
          upload.on('uploaded', function(response) {
            results.push(Files.buildFileResponse(response));
            if (results.length == Object.keys(files).length) {
              return cb(null, results);
            }
          });
          read.pipe(upload);
        }
      });
    } else {
      return cb(null, []);
    }
  },

  Files.remoteMethod('upload', {
    description: 'Uploads a file',
    http: { verb: 'post', path: '/', status: 201 },
    accepts: [
      { arg: 'context', type: 'object', http: { source: 'context' } },
    ],
    returns: [
      { arg: 'name', type: 'string', root: true },
      { arg: 'location', type: 'string', root: true },
    ],
  });

};
