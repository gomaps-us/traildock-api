'use strict';

var shared  = require('../../lib/shared');
var Promise = require('bluebird');

module.exports = function (Aggregate) {

  Aggregate.findAggregate = function (types, filter, options, cb) {
    var promises  = [];
    var typeList  = types.split(',');
    var results   = '';
    for (var i = 0; i < typeList.length; i++) {
      var type  = typeList[i].trim().toLowerCase();
      type      = type.substring(type.length - 1, type.length) === 's' ? type.substring(0, type.length - 1) : type;
      var Model = Aggregate.app.models[type];
      if (Model) {
        promises.push(new Promise(function (resolve, reject) {
          filter = shared.setDefaultFilter(filter, options.accessToken);
          Model.find(filter, function (err, results) {
            if (err) return reject(err);
            return resolve(results);
          });
        }));
      }
    }
    Promise.all(promises).then(function (results) {
      cb(null, results);
    }).catch(function (err) {
      cb(err);
    });
  };

  Aggregate.within = function (types, southWestLatitude, southWestLongitude, northEastLatitude, northEastLongitude, filter, options, cb) {
    var promises  = [];
    var typeList  = types.split(',');
    var results   = '';
    for (var i = 0; i < typeList.length; i++) {
      var type  = typeList[i].trim().toLowerCase();
      type      = type.substring(type.length - 1, type.length) === 's' ? type.substring(0, type.length - 1) : type;
      var Model = Aggregate.app.models[type];
      if (Model) {
        promises.push(new Promise(function (resolve, reject) {
          Model.within(southWestLatitude, southWestLongitude, northEastLatitude, northEastLongitude, filter, options, function (err, results) {
            if (err) return reject(err);
            return resolve(results);
          });
        }));
      }
    }
    Promise.all(promises).then(function (results) {
      cb(null, results);
    }).catch(function (err) {
      cb(err);
    });
  };

  var typesArg = {
    arg: 'types',
    type: 'string',
    http: { source: 'path' },
    required: true,
    description: 'A comma-delimited list of the different types of objects to query, like "reports,trailheads"'
  };

  var withinDefinition = shared.getDefaultWithinDefinition();
  withinDefinition.http.path = '/:types/within';
  withinDefinition.accepts.unshift(typesArg);

  Aggregate.remoteMethod('within', withinDefinition);
  var accepts = [
    { arg: 'filter', type: 'object', description:
    'Filter defining fields, where, include, order, offset, and limit - must be a ' +
    'JSON-encoded string ({"something":"value"})' },
    { arg: 'options', type: 'object', http: 'optionsFromRequest' },
  ];
  accepts.unshift(typesArg);
  Aggregate.remoteMethod('findAggregate', {
    description: 'Find all instances of the models matched by filter from the data source.',
    accessType: 'READ',
    accepts: accepts,
    returns: [{ arg: 'data', type: ['object'], root: true }],
    http: { verb: 'get', path: '/:types' },
  })

};
