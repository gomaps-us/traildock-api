'use strict';

var shared = require('../../lib/shared');
var defaultEnabledMethods = ['create', 'exists', 'findById', 'replaceById',
                             'find', 'findOne', 'deleteById', 'count', 'updatePartial'];

module.exports = function (Model, options) {

  if (Model.modelName == 'common') return;

  shared.disableMethods(Model, (('_enabledMethods' in Model) ? Model._enabledMethods : defaultEnabledMethods));
  shared.addHooks(Model);
  shared.addGeometryRemoteMethods(Model);
  shared.addCommonRemoteMethods(Model);

};
