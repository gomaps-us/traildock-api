FROM node:9

MAINTAINER Traildock

# apt-update and install root dependencies
RUN apt-get update && \
    apt-get install apt-transport-https

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && \
    apt-get install yarn

# create app directory
RUN mkdir -p /srv/traildock/api
WORKDIR /srv/traildock/api

# bundle app source and install dependencies
COPY . /srv/traildock/api
RUN yarn install --production

EXPOSE 9000
CMD [ "yarn", "start" ]
