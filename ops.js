'use strict';

var winston = require('winston-color');
var ops     = require('traildock-node-library').ops;
var path    = require('path');
var fs      = require('fs');
var config  = require('config');

var command = process.argv.splice(2);
winston.info('Running ' + command.join(' '));

var getLimit = function () {
  var limit = null;
  command.forEach(function (item) {
    if (item.match(/^limit=/)) {
      limit = item.replace(/^limit=/, '');
    }
  });
  return limit;
};

if (command[0] == 'test') {
  ops.testrunner(path.resolve(__dirname), {
    type: command.length > 0 ? command[1] : 'all',
    limit: getLimit(),
    containers: ['traildock_api_tests_db', 'traildock_api_tests']
  });
}

if (command[0] == 'crypt' && command.length > 0 && command[1] == 'encrypt-configs') {
  ops.crypt(path.resolve(__dirname)).encryptConfigs();
}
if (command[0] == 'crypt' && command.length > 0 && command[1] == 'decrypt-configs') {
  ops.crypt(path.resolve(__dirname)).decryptConfigs();
}

if (command[0] == 'release') {
  ops.release(
    path.resolve(__dirname),
    'api',
    ops.crypt(path.resolve(__dirname)),
    config.get('dockerhub.organization')
  );
}
