# Traildock HTTP API

An open source API for trail and map data. The source is built on top of [LoopBack](http://loopback.io).

## Dependencies

* Node >=9
* [Yarn](https://yarnpkg.com/en/docs/install). You can use npm if you like locally, but the preferred method is Yarn, and it's what we use for dependency installation and script execution in other environments.
* Docker, see [Docker for Mac](https://www.docker.com/docker-mac) or [Docker for Windows](https://www.docker.com/docker-windows)

## Developing

1. Get the Traildock crypt key from an admin and place it in the root of your clone, in a file named `.cryptkey`
2. Then run the following:
```
yarn install
yarn run develop
```

The default config will be used at `/config/default.json`. If you wish to override during development, you can create a file `/config/develop.json` and override values as needed.

Access the API at http://localhost:9000

## Testing

The project currently includes both unit and functional tests. You can run one or the other or both:

Run all tests:
```
yarn test
```
Run unit tests only:
```
yarn run test-unit
```
Run functional tests only:
```
yarn run test-functional
```

## Deploying

### Configuration

Configuration for all environments lives in `/config/*`. All config files are encrypted since they contain sensitive info like creds for connecting to other services. A given environment knows when it needs to decrypt its relevant config file(s), and attempts to use one of the following to do so:

1. a `.cryptkey` file at the root of the environment's project install
2. an environment variable named `CRYPTKEY`

### Releasing

Just `yarn run release`. It'll tag the repo in its current state, the tag value based on the `package.json` version value, push to the origin source code repo, build the project docker image, and push to the docker image repository.
